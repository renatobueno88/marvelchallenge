//
//  DecodableProtocolTests.swift
//  MarvelChallengeTests
//
//  Created by Renato Souza Bueno on 26/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import XCTest
@testable import MarvelChallenge

class DecodableProtocolTests: XCTestCase {
    
    var decodable: DecodableProtocol?
    var mockedJson: MockJsonService?
    
    func testDecodingSuccess() {
        let data = mockedJson?.buildObject(resource: MockedAction.singleCharacter(id: 0).actionKey, type: "json")
        
        decodable?.decodeObject(data: data, error: nil, type: ResultObject.self, completion: { (result, error) in
            XCTAssertNotNil(result)
            XCTAssertNil(error)
        })
    }
    
    func testDecodingError() {
        let data = mockedJson?.buildObject(resource: MockedAction.singleCharacter(id: 0).actionKey, type: "xml")
        
        decodable?.decodeObject(data: data, error: nil, type: ResultObject.self, completion: { (result, error) in
            XCTAssertNotNil(error)
            XCTAssertNil(result)
        })
    }
    
}
