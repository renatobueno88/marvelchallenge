//
//  FavoritesTest.swift
//  MarvelChallengeTests
//
//  Created by Renato Souza Bueno on 26/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import XCTest
@testable import MarvelChallenge

class FavoritesTest: XCTestCase {
    
    var controller: FavoriteItemsViewController?
    var presenter = FavoriteItemsPresenter(presenterDelegate: nil)
    var mockedCharacters = [MarvelObject]()
    
    func setupController() {
        let controllerIdentifer = String(describing: FavoriteItemsViewController.self)
        controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: controllerIdentifer) as? FavoriteItemsViewController
        presenter = FavoriteItemsPresenter(presenterDelegate: controller, localServiceType: FavoriteMockedLocalService.self)
        controller?.presenter = presenter
    }
    
    override func setUp() {
        super.setUp()
        setupController()
        XCTAssertNotNil(controller)
        UIApplication.shared.keyWindow?.rootViewController = controller
    }
    
    override func tearDown() {
        super.tearDown()
        getMockedCharacter()
    }
    
    func getMockedCharacter() {
        SingleCharacterMockedRequest().fetchSingleCharacter(characterId: 1009742) { (result, _) in
            guard let characters = result?.data?.results else {
                XCTFail()
                return
            }
            self.mockedCharacters = characters
        }
    }
    
    func testEmpty() {
        XCTAssertEqual(presenter.characters, [])
        XCTAssertEqual(presenter.favoriteCharacters, [:])
        XCTAssertEqual(presenter.getCharacterItemDto(atIndex: IndexPath(row: 0, section: 0)).title, "")
        XCTAssertEqual(presenter.getCharacterItemDto(atIndex: IndexPath(row: 0, section: 0)).image, "")
        XCTAssertEqual(presenter.getCharacterItemDto(atIndex: IndexPath(row: 0, section: 0)).isFavorite, false)
        XCTAssertEqual(presenter.getCharacterItemDto(atIndex: IndexPath(row: 0, section: 0)).currentItemId, 0)
        
    }
    
    func testItemDto() {
        getMockedCharacter()
        presenter.localService.saveObject(object: mockedCharacters)
        presenter.loadFavorites()
        let index = IndexPath(row: 0, section: 0)
        XCTAssertEqual(presenter.getCharacterItemDto(atIndex: index).image, "http://i.annihil.us/u/prod/marvel/i/mg/c/d0/4ced5ab9078c9.jpg")
        XCTAssertEqual(presenter.getCharacterItemDto(atIndex: index).title, "Zzzax")
        XCTAssertEqual(presenter.getCharacterItemDto(atIndex: index).isFavorite, true)
        XCTAssertEqual(presenter.getCharacterItemDto(atIndex: index).currentItemId, 1009742)
    }
    
    func testSetup() {
        guard let controller = controller else {
            XCTFail()
            return
        }
        getMockedCharacter()
        controller.presenter.localService.saveObject(object: mockedCharacters)
        controller.presenter.loadFavorites()
        
        XCTAssertEqual(controller.presenter.characters, mockedCharacters)
        XCTAssertEqual(controller.presenter.numberOfSections(), 1)
        XCTAssertEqual(controller.presenter.numberOfRows(inSection: 0), 1)
        XCTAssertEqual(controller.presenter.getFavoritesTitle, "Favoritos")
        if let object = controller.presenter.characters.first {
            XCTAssertEqual(controller.presenter.checkItemIsFavorite(withId: object), true)
        }
        XCTAssertEqual(controller.presenter.favoriteCharacters[1009742], true)
        XCTAssertNil(controller.presenter.favoriteCharacters[123])
        XCTAssertEqual(controller.presenter.getIndexFromFavorite(withId: 1009742), 0)
    }
    
    func testSelectedEntity() {
        guard let controller = controller else {
            XCTFail()
            return
        }
        getMockedCharacter()
        controller.presenter.localService.saveObject(object: mockedCharacters)
        controller.presenter.loadFavorites()
        let index = IndexPath(row: 0, section: 0)
        let selected = presenter.presentSelectedCharacter(atIndex: index)
        XCTAssertNotNil(selected)
        XCTAssertEqual(selected.description, "A chain reaction in an atomic reactor, a result of a terrorist attack, accidentally created a force that absorbed the electrolytes of the nearby humans' brains, granting the explosion of energy a rudimentary sentience.  ")
        XCTAssertEqual(selected.titleName, "Zzzax")
        XCTAssertEqual(selected.thumbnail?.thumbnailPath, "http://i.annihil.us/u/prod/marvel/i/mg/c/d0/4ced5ab9078c9.jpg")
        XCTAssertEqual(selected.id, 1009742)
        XCTAssertEqual(selected.comics?.available, 5)
        XCTAssertEqual(selected.comics?.items?.first?.name, "Hulk (2008) #36")
        XCTAssertEqual(selected.series?.available, 4)
        XCTAssertEqual(selected.series?.items?.first?.name, "Hulk (2008 - 2012)")
    }
}
