//
//  ActionTests.swift
//  MarvelChallengeTests
//
//  Created by Renato Souza Bueno on 26/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import XCTest
@testable import MarvelChallenge

class ActionTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        
    }
    
    func testActionKey() {
        XCTAssertEqual(URLAction.characters.actionKey, "characters")
        XCTAssertEqual(URLAction.comics(id: 0123).actionKey, "characters/\(0123)/comics")
        XCTAssertEqual(URLAction.series(id: 0123).actionKey, "characters/\(0123)/series")
        XCTAssertEqual(URLAction.singleCharacter(id: 0123).actionKey, "characters/\(0123)")
    }
    
    func testMockedActionKey() {
        XCTAssertEqual(MockedAction.characters.actionKey, "charactersList")
        XCTAssertEqual(MockedAction.comics(id: 0).actionKey, "comics")
        XCTAssertEqual(MockedAction.series(id: 0).actionKey, "series")
        XCTAssertEqual(MockedAction.singleCharacter(id: 0).actionKey, "characterDetail")
    }
    
}
