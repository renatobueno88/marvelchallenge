//
//  CharacterDetailTests.swift
//  MarvelChallengeTests
//
//  Created by Renato Souza Bueno on 26/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import XCTest
@testable import MarvelChallenge

class CharacterDetailTests: XCTestCase {

    var controller: CharacterDetailViewController?
    var presenter = CharacterDetailPresenter(presenterDelegate: nil)
    var interactor = CharacterDetailInteractor(interactorDelegate: nil)
    
    var comicsSeriesCellView: ComicsSeriesCellView = ComicsSeriesCellView.loadFromNib()
    var comicsSeriesPresenter = ComicsSeriesCellPresenter()
    
    func setupController() {
        let controllerIdentifer = String(describing: CharacterDetailViewController.self)
        controller = UIStoryboard(name: "CharacterDetail", bundle: nil).instantiateViewController(withIdentifier: controllerIdentifer) as? CharacterDetailViewController
        presenter = CharacterDetailPresenter(localServiceType: FavoriteMockedLocalService.self, presenterDelegate: controller)
        interactor = CharacterDetailInteractor(comicsSeriesRequestType: ComicsSeriesMockedRequest.self, characterDetailRequestType: SingleCharacterMockedRequest.self, interactorDelegate: presenter)
        controller?.presenter = presenter
        controller?.presenter.interactor = interactor
    }
    
    override func setUp() {
        super.setUp()
        setupController()
        XCTAssertNotNil(controller)
        UIApplication.shared.keyWindow?.rootViewController = controller
    }

    func testListControllerSetup() {
        guard let controller = controller else {
            XCTFail()
            return
        }
        controller.presenter.interactor.fetchCharacterDetail(withId: 1009742)
        controller.presenter.fetchComicSeriesIfAvailable()
        XCTAssertEqual(controller.presenter.getCharacterName, "Zzzax")
        XCTAssertEqual(controller.presenter.numberOfRows(inSection: 0), presenter.cellTypes.count)
        XCTAssertEqual(controller.presenter.numberOfSections(), 1)
    }
    
    func testSetup() {
        guard let controller = controller else {
            XCTFail()
            return
        }
        presenter.interactor.fetchCharacterDetail(withId: 1009742)
        XCTAssertEqual(controller.presenter.getImagePath, "http://i.annihil.us/u/prod/marvel/i/mg/c/d0/4ced5ab9078c9.jpg")
        XCTAssertEqual(controller.presenter.getCharacterDescription, "A chain reaction in an atomic reactor, a result of a terrorist attack, accidentally created a force that absorbed the electrolytes of the nearby humans' brains, granting the explosion of energy a rudimentary sentience.  ")
        XCTAssertEqual(controller.presenter.shouldMakeComicsRequest, true)
        XCTAssertEqual(controller.presenter.shouldMakeSeriesRequest, true)
        XCTAssertEqual(controller.presenter.getComics().title, "Quadrinhos")
        XCTAssertEqual(controller.presenter.getSeries().title, "Séries")
        XCTAssertEqual(controller.presenter.productIsFavorite(), false)
    }
    
    func testErrorMessage() {
        XCTAssertEqual(presenter.handleErrorMessage(error: nil, networkError: true), "Ops! Parece que você está sem conexão. Deseja tentar novamente?")
        XCTAssertEqual(presenter.handleErrorMessage(error: nil, networkError: false), "Erro inesperado. Tente Novamente.")
    }
    
    func testCharacterDetail() {
        presenter.interactor.fetchCharacterDetail(withId: 1009742)
        XCTAssertEqual(presenter.character.titleName, "Zzzax")
        XCTAssertEqual(presenter.character.id, 1009742)
        XCTAssertEqual(presenter.character.thumbnail?.thumbnailPath, "http://i.annihil.us/u/prod/marvel/i/mg/c/d0/4ced5ab9078c9.jpg")
        XCTAssertEqual(presenter.character.description, "A chain reaction in an atomic reactor, a result of a terrorist attack, accidentally created a force that absorbed the electrolytes of the nearby humans' brains, granting the explosion of energy a rudimentary sentience.  ")
        XCTAssertEqual(presenter.character.comics?.available, 5)
        XCTAssertEqual(presenter.character.comics?.collectionURI, "http://gateway.marvel.com/v1/public/characters/1009742/comics")
        XCTAssertEqual(presenter.character.comics?.items?.first?.name, "Hulk (2008) #36")
        XCTAssertEqual(presenter.character.comics?.items?.first?.resourceURI, "http://gateway.marvel.com/v1/public/comics/37047")
        XCTAssertEqual(presenter.character.series?.available, 4)
        XCTAssertEqual(presenter.character.series?.items?.first?.name, "Hulk (2008 - 2012)")
        XCTAssertEqual(presenter.character.series?.items?.first?.resourceURI, "http://gateway.marvel.com/v1/public/series/3374")
    }
    
    func testCellType() {
        presenter.interactor.fetchCharacterDetail(withId: 1009742)
        presenter.populateCells()
        var index = IndexPath(row: 0, section: 0)
        XCTAssertEqual(DetailCellType.allCases, presenter.cellTypes)
        XCTAssertEqual(presenter.getCellTypeFromProductType(type: MarvelProductsType.comics), .comics)
        XCTAssertEqual(presenter.getCellTypeFromProductType(type: MarvelProductsType.series), .series)
        XCTAssertEqual(presenter.getCellType(FromIndex: index), .image)
        index = IndexPath(row: 1, section: 0)
        XCTAssertEqual(presenter.estimatedHeightForCell(indexPath: index), UITableView.automaticDimension)
        XCTAssertEqual(presenter.getCellType(FromIndex: index), .description)
        XCTAssertEqual(presenter.estimatedHeightForCell(indexPath: index), UITableView.automaticDimension)
        index = IndexPath(row: 2, section: 0)
        XCTAssertEqual(presenter.getCellType(FromIndex: index), .comics)
        XCTAssertEqual(presenter.estimatedHeightForCell(indexPath: index), 270)
        index = IndexPath(row: 3, section: 0)
        XCTAssertEqual(presenter.getCellType(FromIndex: index), .series)
        XCTAssertEqual(presenter.estimatedHeightForCell(indexPath: index), 270)
    }
    
    func testComics() {
        presenter.interactor.fetchComicsSeries(withProductType: .comics, limit: 20, page: 0, characterId: 72781)
        let comic = presenter.getComics().objects.first
        XCTAssertNotNil(comic)
        XCTAssertEqual(comic?.id, 72781)
        XCTAssertEqual(comic?.imagePath, "http://i.annihil.us/u/prod/marvel/i/mg/3/90/5c9d28089b204.jpg")
        XCTAssertEqual(comic?.title, "Infinity By Starlin & Hickman (Hardcover)")
    }
    
    func testSeries() {
        presenter.interactor.fetchComicsSeries(withProductType: .series, limit: 20, page: 0, characterId: 16450)
        let serie = presenter.getSeries().objects.first
        XCTAssertNotNil(serie)
        XCTAssertEqual(serie?.id, 16450)
        XCTAssertEqual(serie?.imagePath, "http://i.annihil.us/u/prod/marvel/i/mg/5/d0/511e88a20ae34.jpg")
        XCTAssertEqual(serie?.title, "A+X (2012 - Present)")
    }

}
