//
//  TodayExtensionTests.swift
//  MarvelChallengeTests
//
//  Created by Renato Souza Bueno on 26/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import XCTest
@testable import MarvelChallenge

class TodayExtensionTests: XCTestCase {
    
    var presenter = TodayPresenter(presenterDelegate: nil)
    var interactor = ListInteractor(interactorDelegate: nil)
    
    override func setUp() {
        super.setUp()
        interactor = ListInteractor(listServiceType: ListMockedRequest.self, interactorDelegate: presenter)
        presenter.interactor = interactor
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testSetup() {
        presenter.loadContent(limit: 3)
        XCTAssertEqual(presenter.numberOfSections(), 1)
        XCTAssertEqual(presenter.numberOfRows(inSection: 0), presenter.characters.count)
        XCTAssertEqual(presenter.characters.count, 20)
        XCTAssertEqual(presenter.characters.last?.id, 1010870)
        XCTAssertEqual(presenter.characters.last?.titleName, "Ajaxis")
        XCTAssertEqual(presenter.characters.last?.description, "")
        XCTAssertEqual(presenter.characters.last?.thumbnail?.thumbnailPath, "http://i.annihil.us/u/prod/marvel/i/mg/b/70/4c0035adc7d3a.jpg")
        XCTAssertEqual(presenter.characters.last?.comics?.available, 0)
        XCTAssertNil(presenter.characters.last?.comics?.items?.first?.name)
        XCTAssertNil(presenter.characters.last?.comics?.items?.first?.resourceURI)
        XCTAssertEqual(presenter.characters.last?.series?.available, 0)
        XCTAssertNil(presenter.characters.last?.series?.items?.first?.name)
        XCTAssertNil(presenter.characters.last?.series?.items?.first?.resourceURI)
    }
}

