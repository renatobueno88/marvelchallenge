//
//  CharactersListTests.swift
//  CharactersListTests
//
//  Created by Renato Souza Bueno on 21/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import XCTest
@testable import MarvelChallenge

class NetworkerProtocolTests: XCTestCase {
    
    var networker: NetworkerProtocol = MockedRequest(urlString: "")
    
    func testParametersKeys() {
        print(networker.defaultQueryParameters())
        XCTAssertTrue(networker.defaultQueryParameters().contains("ts="))
        XCTAssertTrue(networker.defaultQueryParameters().contains("&apikey="))
        XCTAssertTrue(networker.defaultQueryParameters().contains("&hash="))
        networker = NetworkRequest(urlString: Environment.baseUrl)
        XCTAssertEqual(Environment.baseUrl, "http://gateway.marvel.com/v1/public/")
        XCTAssertTrue(networker.defaultQueryParameters().contains("ts="))
        XCTAssertTrue(networker.defaultQueryParameters().contains("&apikey="))
        XCTAssertTrue(networker.defaultQueryParameters().contains("&hash="))
    }
    
}

class CharactersListTests: XCTestCase {

    var controller: CharactersViewController?
    var presenter = CharacterListPresenter(presenterDelegate: nil)
    var interactor = ListInteractor(interactorDelegate: nil)
    
    func setupController() {
        let controllerIdentifer = String(describing: CharactersViewController.self)
        controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: controllerIdentifer) as? CharactersViewController
        presenter = CharacterListPresenter(presenterDelegate: controller, localServiceType: FavoriteMockedLocalService.self)
        interactor = ListInteractor(listServiceType: ListMockedRequest.self, interactorDelegate: presenter)
        controller?.presenter = presenter
        controller?.presenter.interactor = interactor
    }
    
    override func setUp() {
        super.setUp()
        setupController()
        XCTAssertNotNil(controller)
        UIApplication.shared.keyWindow?.rootViewController = controller
    }
   
    override func tearDown() {
    }
    
    func testListControllerSetup() {
        guard let controller = controller else {
            XCTFail()
            return
        }
        controller.presenter.loadContent()
        XCTAssertEqual(controller.presenter.charactersListTitle, "Personagens")
        XCTAssertEqual(controller.presenter.numberOfRows(inSection: 0), presenter.characters.count)
        XCTAssertEqual(controller.presenter.numberOfSections(), 1)
    }
    
    func testItemDto() {
        guard let controller = controller else {
            XCTFail()
            return
        }
        let index = IndexPath(row: 0, section: 0)
        let itemDto = controller.presenter.getCharacterItemDto(atIndex: index)
        
        XCTAssertEqual(itemDto.currentItemId, 1011334)
        XCTAssertEqual(itemDto.title, "3-D Man")
        XCTAssertEqual(itemDto.isFavorite, false)
        XCTAssertEqual(itemDto.image, "http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784.jpg")
    }
    
    func testErrorMessage() {
        XCTAssertEqual(presenter.handleErrorMessage(error: nil, networkError: true), "Ops! Parece que você está sem conexão. Deseja tentar novamente?")
        XCTAssertEqual(presenter.handleErrorMessage(error: nil, networkError: false), "Erro inesperado. Tente Novamente.")
    }
    
    func testSelectedEntity() {
        let index = IndexPath(row: 1, section: 0)
        let selected = presenter.presentSelectedCharacter(atIndex: index)
        XCTAssertNotNil(selected)
        XCTAssertEqual(selected.description, "Rick Jones has been Hulk's best bud since day one, but now he's more than a friend...he's a teammate! Transformed by a Gamma energy explosion, A-Bomb's thick, armored skin is just as strong and powerful as it is blue. And when he curls into action, he uses it like a giant bowling ball of destruction! ")
        XCTAssertEqual(selected.titleName, "A-Bomb (HAS)")
        XCTAssertEqual(selected.thumbnail?.thumbnailPath, "http://i.annihil.us/u/prod/marvel/i/mg/3/20/5232158de5b16.jpg")
        XCTAssertEqual(selected.id, 1017100)
        XCTAssertEqual(selected.comics?.available, 3)
        XCTAssertEqual(selected.comics?.items?.first?.name, "Hulk (2008) #53")
        XCTAssertEqual(selected.series?.items?.first?.name, "Hulk (2008 - 2012)")
    }
}
