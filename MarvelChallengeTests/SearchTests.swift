//
//  SearchTests.swift
//  MarvelChallengeTests
//
//  Created by Renato Souza Bueno on 26/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import XCTest
@testable import MarvelChallenge

class SearchTests: XCTestCase {
    
    var controller: SearchViewController?
    var presenter = SearchPresenter(presenterDelegate: nil)
    var interactor = SearchCharactersInteractor(interactorDelegate: nil)
    
    func setupController() {
        let controllerIdentifer = String(describing: SearchViewController.self)
        controller = UIStoryboard(name: "Search", bundle: nil).instantiateViewController(withIdentifier: controllerIdentifer) as? SearchViewController
        presenter = SearchPresenter(presenterDelegate: controller, localServiceType: FavoriteMockedLocalService.self)
        interactor = SearchCharactersInteractor(searchServiceType: SearchMockedRequest.self, interactorDelegate: presenter)
        controller?.presenter = presenter
        controller?.presenter.interactor = interactor
    }
    
    override func setUp() {
        super.setUp()
        setupController()
        XCTAssertNotNil(controller)
        UIApplication.shared.keyWindow?.rootViewController = controller
    }
    
    override func tearDown() {
    }
    
    func testSetup() {
        guard let controller = controller else{
            XCTFail()
            return
        }
        controller.presenter.searchTerm = "3-D Man"
        controller.loadSearch(page: 0)
        XCTAssertEqual(presenter.searchTermType, .name)
        XCTAssertEqual(presenter.searchTermType.queryKey, "&name=")
        XCTAssertEqual(controller.presenter.numberOfSections(), 1)
        XCTAssertEqual(controller.presenter.numberOfRows(inSection: 0), controller.presenter.characters.count)
        XCTAssertEqual(controller.presenter.getCurrentSearchTerm, "3-D Man")
        XCTAssertEqual(controller.page, 0)
    }
    
    func testDtoSearchSetup() {
        presenter.searchTerm = "3-D Man"
        presenter.makeSearch(page: 0, limit: 20)
        let character = presenter.characters.first
        XCTAssertNotNil(character)
        let index = IndexPath(row: 0, section: 0)
        XCTAssertEqual(presenter.getCharacterItemDto(atIndex: index).currentItemId, 1011334)
        XCTAssertEqual(presenter.getCharacterItemDto(atIndex: index).image, "http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784.jpg")
        XCTAssertEqual(presenter.getCharacterItemDto(atIndex: index).isFavorite, false)
        XCTAssertEqual(presenter.getCharacterItemDto(atIndex: index).title, "3-D Man")
    }
    
    func testSearchType() {
        XCTAssertEqual(SearchTermType.name.queryKey, "&name=")
        XCTAssertEqual(SearchTermType.nameStartsWith.queryKey, "&nameStartsWith=")
    }
    
    func testSelectedEntity() {
        presenter.searchTerm = "3-D Man"
        presenter.makeSearch(page: 0, limit: 20)
        let index = IndexPath(row: 0, section: 0)
        let selected = presenter.presentSelectedCharacter(atIndex: index)
        XCTAssertNotNil(selected)
        XCTAssertEqual(selected.description, "")
        XCTAssertEqual(selected.titleName, "3-D Man")
        XCTAssertEqual(selected.thumbnail?.thumbnailPath, "http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784.jpg")
        XCTAssertEqual(selected.id, 1011334)
        XCTAssertEqual(selected.comics?.available, 12)
        XCTAssertEqual(selected.comics?.items?.first?.name,"Avengers: The Initiative (2007) #14")
        XCTAssertEqual(selected.series?.available, 3)
        XCTAssertEqual(selected.series?.items?.first?.name, "Avengers: The Initiative (2007 - 2010)")
    }
    
}

