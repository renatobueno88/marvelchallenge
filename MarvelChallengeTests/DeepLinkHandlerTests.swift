//
//  DeepLinkHandlerTests.swift
//  MarvelChallengeTests
//
//  Created by Renato Souza Bueno on 26/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import XCTest
@testable import MarvelChallenge

class DeepLinkHandlerTests: XCTestCase {
    
    var handler: DeeplinkHandler = DeeplinkHandler()
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testDeepLinkHandleFail() {
        let url = URL(string: "testUrl")
        XCTAssertFalse(handler.application(UIApplication.shared, open: url!, options: [:]))
    }
    
    func testDeepLinkHandleSuccess() {
        XCTAssertEqual(Environment.schemeUrl, "MARVEL-CHALLENGE://")
        guard let url = URL(string: "MARVEL-CHALLENGE://1017100") else {
            XCTFail()
            return
        }
        XCTAssertTrue(handler.application(UIApplication.shared, open: url, options: [:]))
    }
    
}
