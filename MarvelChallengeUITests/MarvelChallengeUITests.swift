//
//  MarvelChallengeUITests.swift
//  MarvelChallengeUITests
//
//  Created by Renato Souza Bueno on 26/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import XCTest

class MarvelChallengeUITests: XCTestCase {

    override func setUp() {
        continueAfterFailure = false
    }

    override func tearDown() {

    }
    
    func testSimpleSearchWithFavoriteAction() {
        let app = XCUIApplication()
        app.launch()
        sleep(3)
        app.searchFields["Buscar personagem"].tap()
        app.searchFields["Buscar personagem"].typeText("Thor")
        sleep(2)
        
        app/*@START_MENU_TOKEN@*/.buttons["Search"]/*[[".keyboards",".buttons[\"Buscar\"]",".buttons[\"Search\"]"],[[[-1,2],[-1,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/.tap()
        sleep(5)
        app.collectionViews/*@START_MENU_TOKEN@*/.buttons["ItemCellView.favoriteButton"]/*[[".cells",".buttons[\"favorite off\"]",".buttons[\"ItemCellView.favoriteButton\"]"],[[[-1,2],[-1,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/.tap()
        sleep(4)
        let tabBarsQuery = app.tabBars
        tabBarsQuery.buttons["Favoritos"].tap()
        sleep(5)
        let cell = app.collectionViews.children(matching: .cell)
        sleep(5)
        cell.element(boundBy: 0).tap()
        sleep(3)
        app.navigationBars.buttons["favoriteNav on"].tap()
        sleep(3)
        app.navigationBars.buttons["Favoritos"].tap()
        
    }
    
}
