//
//  TodayPresenter.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 25/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

protocol TodayPresenterDelegate: class {
    func shouldShowError(withMessage message: String)
    func didFinishRequest()
    func presentEmptyResults()
}

class TodayPresenter: ListInteractorDelegate, ErrorMessagingProtocol {
    
    weak var presenterDelegate: TodayPresenterDelegate?
    var characters = [MarvelObject]()
    var resultsData = DataObject()
    lazy var interactor: CharacterListProtocol = ListInteractor(interactorDelegate: self)
    
    init(presenterDelegate: TodayPresenterDelegate?) {
        self.presenterDelegate = presenterDelegate
    }
    
    func loadContent(limit: Int = 3) {
        interactor.fetchCharacters(page: 0, limit: limit)
    }
    
    func getCharacterItemDto(atIndex indexPath: IndexPath) -> ItemCellDto {
        guard !characters.isEmpty else {
            return ItemCellDto()
        }
        let character = characters[indexPath.row]
        return ItemCellDto(isFavorite: false, currentItemId: character.id, image: character.thumbnail?.thumbnailPath ?? "", title: character.titleName)
    }
    
    func presentSelectedCharacter(atIndex indexPath: IndexPath) -> MarvelObject {
        return characters[indexPath.row]
    }
    
    func appendRemovingDuplicates(results: [MarvelObject]) {
        results.forEach { (character) in
            guard !characters.contains(where: { $0.id == character.id }) else {
                return
            }
            characters.append(character)
        }
    }
    
    // MARK: ListInteractorDelegate
    
    func didFinishRequest(withEntity entity: DataObject) {
        self.resultsData = entity
        appendRemovingDuplicates(results: entity.results)
        if entity.count == 0 {
            self.presenterDelegate?.presentEmptyResults()
        } else {
            self.presenterDelegate?.didFinishRequest()
        }
    }
    
    func shouldShowError(error: Error?, networkError: Bool) {
        let message = handleErrorMessage(error: error, networkError: networkError)
        self.presenterDelegate?.shouldShowError(withMessage: message)
    }
    
    // MARK: CellSetup
    
    func numberOfSections() -> Int {
        return 1
    }
    
    func numberOfRows(inSection section: Int) -> Int {
        return characters.count
    }
    
}
