//
//  TodayViewController.swift
//  CharactersWidgetExtension
//
//  Created by Renato Souza Bueno on 24/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit
import NotificationCenter

class TodayViewController: UIViewController, NCWidgetProviding, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, TodayPresenterDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var messageLabel: UILabel!
    lazy var presenter = TodayPresenter(presenterDelegate: self)

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        messageLabel.text = ""
        presenter.loadContent()
        self.extensionContext?.widgetLargestAvailableDisplayMode = .expanded
        self.preferredContentSize = CGSize(width: self.view.frame.size.width, height: 210)
    }
    
    func createCharacterCell(collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: ItemCollectionViewCell = ItemCollectionViewCell.createCell(collectionView: collectionView, indexPath: indexPath)
        cell.view.backgroundColor = .clear
        cell.fill(dto: presenter.getCharacterItemDto(atIndex: indexPath), favoriteDelegate: nil, shouldHideFavoriteIcon: true)
        return cell
    }
    
    //MARK: UICollectionViewDelegate, UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return createCharacterCell(collectionView: collectionView, cellForItemAt: indexPath)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return presenter.numberOfSections()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.numberOfRows(inSection: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.collectionView?.calculateSizeCell(numberOfCells: 3, cellSpace: 0) ?? .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedCharacter = presenter.presentSelectedCharacter(atIndex: indexPath)
        showCharacterDetail(character: selectedCharacter)
    }
    
    func showCharacterDetail(character: MarvelObject) {
        let schemeUrl = Environment.schemeUrl + "d/id/\(character.id)"
        guard let url = URL(string: schemeUrl) else {
            return
        }
        self.extensionContext?.open(url, completionHandler: nil)
    }
        
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        completionHandler(NCUpdateResult.newData)
    }
    
    func widgetActiveDisplayModeDidChange(_ activeDisplayMode: NCWidgetDisplayMode, withMaximumSize maxSize: CGSize) {
        if activeDisplayMode == .expanded {
            self.preferredContentSize = CGSize(width: self.view.frame.size.width, height: 220)
        } else if activeDisplayMode == .compact {
            self.preferredContentSize = CGSize(width: maxSize.width, height: 110)
        }
    }
    
    // MARK: TodayPresenterDelegate
    
    func didFinishRequest() {
        DispatchQueue.main.async {
            self.messageLabel.text = ""
            self.collectionView.reloadData()
        }
    }
    
    func presentEmptyResults() {
        let message = "Ops! Parece que não houveram resultados"
        self.messageLabel.text = message
    }
    
    func shouldShowError(withMessage message: String) {
        self.messageLabel.text = message
    }
    
}
