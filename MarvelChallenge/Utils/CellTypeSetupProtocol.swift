//
//  CellTypeSetupProtocol.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 22/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

protocol TransporterProtocol {
    func transporter(object: Transporter<Any>)
}

class Transporter<T> {
    var data: T!
    
    init(data: T) {
        self.data = data
    }
    
    init() {
        
    }
}

protocol CellTypeSetupProtocol: CellSetupProtocol {
    associatedtype CellType
    var cellTypes: [CellType] { get set }
}

protocol CellSetupProtocol {
    func numberOfRows(inSection section: Int) -> Int
    func numberOfSections() -> Int
    func estimatedHeightForCell(indexPath: IndexPath) -> CGFloat
}
