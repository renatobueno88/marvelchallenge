//
//  UIView+extensions.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 22/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

enum ImageAsset: String {
    case favoriteOn = "favorite_on"
    case favoriteOff = "favorite_off"
    case favoriteIcon = "favoriteNav_off"
    case favoriteIconOn = "favoriteNav_on"
    case homeIcon = "Home"
    
}

protocol CalculateCellSizeProtocol {
    func calculateSizeCell(numberOfCells: Float, cellSpace: CGFloat?) -> CGSize
}

extension UIView: CalculateCellSizeProtocol {
    func calculateSizeCell(numberOfCells: Float, cellSpace: CGFloat?) -> CGSize {
        let screenValueToCalculate = 0.9465240642
        var viewWidth = self.frame.size.width
        if let cellSpace = cellSpace {
            viewWidth -= cellSpace
        }
        let width = viewWidth > 0 ? Double(viewWidth / CGFloat(numberOfCells)) : Double(1 / CGFloat(numberOfCells))
        
        let height = Double(width * screenValueToCalculate + 105)
        return CGSize(width: width, height: height)
    }
}

extension UIButton {
    private var allStates: [UIControl.State] {
        return [.normal, .highlighted, .selected, .disabled]
    }
    
    func setImageForAllStates(_ image: ImageAsset?) {
        let assetImage = UIImage(named: image?.rawValue ?? "")
        allStates.forEach { setImage(assetImage, for: $0) }
    }
    
}

extension UIView {
    
    func circleView(withBackgroundColor: UIColor? = nil) {
        if let bgColor = withBackgroundColor {
            self.backgroundColor = bgColor
        }
        self.layer.cornerRadius = self.frame.size.width / 2
        self.clipsToBounds = true
    }
    
    class func loadFromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)!.first as! T
    }
    
    func fillWithSubview(subview: UIView) {
        subview.translatesAutoresizingMaskIntoConstraints = false
        addSubview(subview)
        
        let views = ["view": subview]
        let vC = NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|", options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: views)
        let hC = NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|", options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: views)
        
        addConstraints(vC)
        addConstraints(hC)
        updateConstraints()
        
        subview.setNeedsDisplay()
        layoutIfNeeded()
    }
    
}
