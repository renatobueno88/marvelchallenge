//
//  UIViewController+extensions.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 22/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showDefaultSystemAlert(message: String, actionTitle: String = "Tentar novamente", cancelTitle: String = "Cancelar", okActionHandler: ((UIAlertAction) -> Void)?) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Alerta", message: message, preferredStyle: .alert)
            if !cancelTitle.isEmpty {
                let dismissAction = UIAlertAction(title: cancelTitle, style: .cancel, handler: nil)
                alert.addAction(dismissAction)
            }
            if !actionTitle.isEmpty {
                let action = UIAlertAction(title: actionTitle, style: .default, handler: okActionHandler)
                alert.addAction(action)
            }
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func setupFavoriteOnNavigation(isFavorite: Bool) -> UIBarButtonItem {
        let image = isFavorite ? ImageAsset.favoriteIconOn : ImageAsset.favoriteIcon
        let favImage = UIImage(named: image.rawValue)?.withRenderingMode(.alwaysTemplate)
        let favItem = UIBarButtonItem(image: favImage,
                                       style: .plain,
                                       target: self,
                                       action: #selector(addToFavoriteAction))
        return favItem
    }
    
    @objc func addToFavoriteAction() { }
}
