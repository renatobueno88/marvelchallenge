//
//  ErrorMessagingProtocol.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 25/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

protocol ErrorMessagingProtocol {
    func handleErrorMessage(error: Error?, networkError: Bool) -> String
}
extension ErrorMessagingProtocol {
    func handleErrorMessage(error: Error?, networkError: Bool) -> String {
        if let error = error, !networkError {
            return error.localizedDescription
        } else {
            let defaultMessage = "Erro inesperado. Tente Novamente."
            let networkErrorMessage = "Ops! Parece que você está sem conexão. Deseja tentar novamente?"
            return networkError ? networkErrorMessage : defaultMessage
        }
    }
}
