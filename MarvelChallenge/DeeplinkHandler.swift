//
//  DeeplinkHandler.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 26/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

enum ControllerType {
    case detail
    case list
}

class DeeplinkHandler: AppDelegate {
    
    func showController(withUrl url: URL, type: ControllerType) {
        switch type {
        case .detail:
            let urlPath = url.absoluteString
            showDetail(urlPath: urlPath)
        case .list:
            showList()
        }
    }
    
    func showDetail(urlPath: String) {
        guard let characterId = Int(urlPath.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()) else {
            return
        }
        CustomTabBarController.sharedInstance?.listInstance?.showCharacterDetail(character: nil, characterId: characterId)
    }
    
    func showList() {
        guard let controller = CustomTabBarController.sharedInstance?.listInstance else {
            return
        }
        window?.rootViewController = controller
        window?.makeKeyAndVisible()
    }
    
}
