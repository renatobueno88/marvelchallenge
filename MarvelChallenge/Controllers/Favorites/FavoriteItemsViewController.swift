//
//  FavoriteItemsViewController.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 23/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

class FavoriteItemsViewController: UIViewController, CollectionViewProtocols, FavoriteItemsPresenterDelegate, ItemFavoriteDelegate, DefaultItemCellSetupProtocol {
    
    @IBOutlet weak var collectionView: UICollectionView!
    lazy var presenter = FavoriteItemsPresenter(presenterDelegate: self)

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.title = presenter.getFavoritesTitle
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter.loadFavorites()
    }
    
    // MARK: DefaultItemCellSetupProtocol
    
    func createCharacterCell(collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = createCharacterCell(collectionView: collectionView, cellForItemAt: indexPath, dto: presenter.getCharacterItemDto(atIndex: indexPath), favoriteDelegate: self)
        return cell
    }
    
    // MARK: CollectionViewProtocols
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return createCharacterCell(collectionView: collectionView, cellForItemAt: indexPath)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return presenter.numberOfSections()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.numberOfRows(inSection: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let character = presenter.presentSelectedCharacter(atIndex: indexPath)
        self.showCharacterDetail(character: character)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.collectionView?.calculateSizeCell(numberOfCells: 2, cellSpace: nil) ?? CGSize(width: 0, height: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell = cell as? ItemCollectionViewCell {
            cell.cancelDownloadTask()
        }
    }
    
    func reloadCollectionView() {
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    // MARK: ItemFavoriteDelegate
    
    func didSelectedFavorite(withId id: Int, isSelected: Bool) {
        if isSelected {
            presenter.removeItemAsFavorite(withId: id)
        } else {
            presenter.saveItemAsFavorite(withId: id)
        }
        presenter.loadFavorites()
        guard let row = presenter.getIndexFromFavorite(withId: id) else {
            return
        }
        let indexPath = IndexPath(row: row, section: 0)
        DispatchQueue.main.async {
            guard self.collectionView.hasRowAtIndexPath(indexPath: indexPath) else {
                return
            }
            if isSelected {
                self.reloadCollectionView()
            } else {
                self.collectionView.reloadItems(at: [indexPath])
            }
        }
    }
    
    // MARK: FavoriteItemsPresenterDelegate
    
    func didFinishRequest() {
        self.reloadCollectionView()
    }
    
    func presentEmptyResults() {
        let message = "Ops! Parece que você ainda não adicionou nenhum favorito"
        self.showDefaultSystemAlert(message: message, actionTitle: "OK", cancelTitle: "") { (_) in
            CustomTabBarController.sharedInstance?.switchTabBar(selected: .list)
        }
    }
    
    // MARK: Navigation
    
    func showCharacterDetail(character: MarvelObject?) {
        guard let controller = DetailControllerBuilder(selectedCharacter: character).build() else {
            return
        }
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }

}
