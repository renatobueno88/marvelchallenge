//
//  FavoriteItemsPresenter.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 23/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

protocol FavoriteItemsPresenterDelegate: class {
    func didFinishRequest()
    func presentEmptyResults()
}

class FavoriteItemsPresenter: AddToFavoriteProtocol {
    
    private weak var presenterDelegate: FavoriteItemsPresenterDelegate?
    private var localServiceType: FavoriteLocalProtocol.Type
    var localService: FavoriteLocalProtocol {
        return localServiceType.init(favoriteKey: "UserFavorites")
    }
    var characters = [MarvelObject]()
    var favoriteCharacters = [Int: Bool]()
    
    init(presenterDelegate: FavoriteItemsPresenterDelegate?, localServiceType: FavoriteLocalProtocol.Type = FavoriteLocalService.self) {
        self.presenterDelegate = presenterDelegate
        self.localServiceType = localServiceType
    }
    
    var getFavoritesTitle: String {
        return "Favoritos"
    }
    
    func loadFavorites() {
        guard let favorites = localService.queryObject() else {
            presenterDelegate?.presentEmptyResults()
            return
        }
        characters = []
        favorites.forEach { (favorite) in
            guard !characters.contains(where: { $0.id == favorite.id }) else {
                return
            }
            favoriteCharacters[favorite.id] = true
            characters.append(favorite)
        }
        
        presenterDelegate?.didFinishRequest()
    }
    
    func getCharacterItemDto(atIndex indexPath: IndexPath) -> ItemCellDto {
        guard !characters.isEmpty else {
            return ItemCellDto()
        }
        let character = characters[indexPath.row]
        let itemIsFavorite = favoriteCharacters[character.id] ?? false
        return ItemCellDto(isFavorite: itemIsFavorite, currentItemId: character.id, image: character.thumbnail?.thumbnailPath ?? "", title: character.titleName)
    }
    
    func presentSelectedCharacter(atIndex indexPath: IndexPath) -> MarvelObject {
        return characters[indexPath.row]
    }
    
    // MARK: CellSetup
    
    func numberOfSections() -> Int {
        return 1
    }
    
    func numberOfRows(inSection section: Int) -> Int {
        return characters.count
    }
    
}
