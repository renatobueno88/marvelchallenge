//
//  AddToFavoriteProtocol.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 23/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

protocol AddToFavoriteProtocol: class, ItemsPresenterPropertiesProtocol {
    var favoriteCharacters: [Int: Bool] { get set }
    var localService: FavoriteLocalProtocol { get }
    func loadFavorites()
    func saveItemAsFavorite(withId id: Int)
    func removeItemAsFavorite(withId id: Int)
    func checkItemIsFavorite(withId object: MarvelObject) -> Bool
    func getIndexFromFavorite(withId id: Int) -> Int?
}

extension AddToFavoriteProtocol {
    func loadFavorites() {
        guard let favorites = localService.queryObject() else {
            return
        }
        favorites.forEach { (favorite) in
            self.favoriteCharacters[favorite.id] = true
        }
    }
    
    func saveItemAsFavorite(withId id: Int) {
        guard let characterToSave = characters.first(where: { $0.id == id }) else {
            favoriteCharacters[id] = false
            return
        }
        favoriteCharacters[id] = true
        var localArray: [MarvelObject] = []
        
        if let localCharacters = localService.queryObject(), !localCharacters.isEmpty {
            localArray.append(contentsOf: localCharacters)
        } else {
            localArray.append(characterToSave)
        }
        localArray.forEach { (localObject) in
            if localObject.id != characterToSave.id {
                localArray.append(characterToSave)
            }
        }
        
        localService.saveObject(object: localArray)
    }
    
    func removeItemAsFavorite(withId id: Int) {
        guard let characterToReplace = characters.first(where: { $0.id == id }) else {
            favoriteCharacters[id] = false
            return
        }
        
        favoriteCharacters[id] = false
        var localArray: [MarvelObject] = []
        
        if let localCharacters = localService.queryObject() {
            localArray.append(contentsOf: localCharacters)
        }
        localArray.removeAll(where: { $0.id == characterToReplace.id })
        
        localService.saveObject(object: localArray)
    }
    
    func getIndexFromFavorite(withId id: Int) -> Int? {
        return characters.firstIndex(where: { $0.id == id })
    }
    
    func checkItemIsFavorite(withId object: MarvelObject) -> Bool {
        guard let object = localService.querySingleObject(id: object.id) else {
            return false
        }
        return favoriteCharacters[object.id] ?? false
    }
}

protocol ItemsPresenterPropertiesProtocol {
    var characters: [MarvelObject] { get set }
    var resultsData: DataObject { get set }
    var totalPages: Int? { get set }
    var contentIsLoading: Bool { get set }
}
extension ItemsPresenterPropertiesProtocol {
    
    var characters: [MarvelObject] {
        get {
            return []
        } set {
            characters.append(contentsOf: newValue)
        }
    }
    
    var resultsData: DataObject {
        get {
            return DataObject()
        } set {
            resultsData = newValue
        }
    }
    
    var totalPages: Int? {
        get {
            return nil
        } set {
            totalPages = newValue
        }
    }
    
    var contentIsLoading: Bool {
        get {
            return false
        } set {
            contentIsLoading = newValue
        }
    }
    
}
