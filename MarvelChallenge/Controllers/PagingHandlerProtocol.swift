//
//  PagingHandlerProtocol.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 24/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

protocol PagingHandlerProtocol: class {
    func handlePaging(page: Int?, totalPages: Int?, refreshContent: Bool) -> Int
    var shouldStopPaging: Bool { get set }
}
extension PagingHandlerProtocol {
    func handlePaging(page: Int?, totalPages: Int?, refreshContent: Bool = false) -> Int {
        var pageOffset = page ?? 0
        self.shouldStopPaging = false
        if let totalPages = totalPages, pageOffset + 1 > totalPages {
            self.shouldStopPaging = true
            return pageOffset
        }
        if refreshContent {
            pageOffset = 0
        }
        if page != nil {
            pageOffset = pageOffset + 1
        }
        return pageOffset
    }
}
