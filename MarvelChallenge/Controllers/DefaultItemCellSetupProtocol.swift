//
//  DefaultItemCellSetupProtocol.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 26/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

protocol DefaultItemCellSetupProtocol {
    func createCharacterCell(collectionView: UICollectionView, cellForItemAt indexPath: IndexPath, dto: ItemCellDto, favoriteDelegate: ItemFavoriteDelegate?, shouldHideFavoriteIcon: Bool) -> UICollectionViewCell
}
extension DefaultItemCellSetupProtocol {
    func createCharacterCell(collectionView: UICollectionView, cellForItemAt indexPath: IndexPath, dto: ItemCellDto, favoriteDelegate: ItemFavoriteDelegate? = nil, shouldHideFavoriteIcon: Bool = false) -> UICollectionViewCell {
        let cell: ItemCollectionViewCell = ItemCollectionViewCell.createCell(collectionView: collectionView, indexPath: indexPath)
        cell.fill(dto: dto, favoriteDelegate: favoriteDelegate, shouldHideFavoriteIcon: shouldHideFavoriteIcon)
        return cell
    }
}
