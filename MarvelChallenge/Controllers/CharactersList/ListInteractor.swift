//
//  ListInteractor.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 22/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

protocol ListInteractorDelegate: class {
    func didFinishRequest(withEntity entity: DataObject)
    func shouldShowError(error: Error?, networkError: Bool)
}

protocol CharacterListProtocol {
    func fetchCharacters(page: Int, limit: Int)
}

class ListInteractor: CharacterListProtocol {
    
    private var listServiceType: ListRequestService.Type
    private var listRequest: ListRequestService {
        return listServiceType.init()
    }
    private weak var interactorDelegate: ListInteractorDelegate?
    
    init(listServiceType: ListRequestService.Type = ListRequest.self, interactorDelegate: ListInteractorDelegate?) {
        self.interactorDelegate = interactorDelegate
        self.listServiceType = listServiceType
    }
    
    // MARK: CharacterListProtocol
    
    func fetchCharacters(page: Int, limit: Int) {
        listRequest.fetchCharacters(paging: page, limit: limit) { (resultsData, error) in
            guard let resultsData = resultsData, let data = resultsData.data, error == nil else {
                let hasNetworkError = !(Reachability.isConnectedToNetwork())
                self.interactorDelegate?.shouldShowError(error: error, networkError: hasNetworkError)
                return
            }
            self.interactorDelegate?.didFinishRequest(withEntity: data)
        }
    }
    
}
