//
//  CharacterListPresenter.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 22/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

protocol ListPresenterDelegate: class {
    func shouldShowError(withMessage message: String)
    func didFinishRequest(currentPage page: Int)
    func presentEmptyResults()
}

class CharacterListPresenter: ListInteractorDelegate, AddToFavoriteProtocol, PagingHandlerProtocol, ErrorMessagingProtocol {
    
    private weak var presenterDelegate: ListPresenterDelegate?
    lazy var interactor: CharacterListProtocol = ListInteractor(interactorDelegate: self)
    private var localServiceType: FavoriteLocalProtocol.Type
    var localService: FavoriteLocalProtocol {
        return localServiceType.init(favoriteKey: "UserFavorites")
    }
    var favoriteCharacters = [Int: Bool]()
    var shouldStopPaging: Bool = false
    
    // MARK: ItemsPresenterPropertiesProtocol
    
    var characters = [MarvelObject]()
    var resultsData = DataObject()
    var totalPages: Int?
    var contentIsLoading = false
    
    init(presenterDelegate: ListPresenterDelegate?, localServiceType: FavoriteLocalProtocol.Type = FavoriteLocalService.self) {
        self.presenterDelegate = presenterDelegate
        self.localServiceType = localServiceType
    }
    
    func loadContent(page: Int? = nil, limit: Int = 20, refreshContent: Bool = false) {
        let pageOffset = handlePaging(page: page, totalPages: totalPages, refreshContent: refreshContent)
        if shouldStopPaging {
            self.presenterDelegate?.didFinishRequest(currentPage: pageOffset)
            contentIsLoading = false
            return
        }
        contentIsLoading = true
        interactor.fetchCharacters(page: pageOffset, limit: limit)
    }
    
    var charactersListTitle: String {
        return "Personagens"
    }
    
    func getCharacterItemDto(atIndex indexPath: IndexPath) -> ItemCellDto {
        guard !characters.isEmpty else {
            return ItemCellDto()
        }
        let character = characters[indexPath.row]
        let itemIsFavorite = checkItemIsFavorite(withId: character)
        return ItemCellDto(isFavorite: itemIsFavorite, currentItemId: character.id, image: character.thumbnail?.thumbnailPath ?? "", title: character.titleName)
    }
    
    func presentSelectedCharacter(atIndex indexPath: IndexPath) -> MarvelObject {
        return characters[indexPath.row]
    }
    
    func appendRemovingDuplicates(results: [MarvelObject]) {
        results.forEach { (character) in
            guard !characters.contains(where: { $0.id == character.id }) else {
                return
            }
            characters.append(character)
        }
    }
    
    // MARK: ListInteractorDelegate
    
    func didFinishRequest(withEntity entity: DataObject) {
        self.resultsData = entity
        self.totalPages = entity.total
        appendRemovingDuplicates(results: entity.results)
        if entity.count == 0 {
            self.presenterDelegate?.presentEmptyResults()
        } else {
            self.presenterDelegate?.didFinishRequest(currentPage: entity.offset)
        }
        contentIsLoading = false
    }
    
    func shouldShowError(error: Error?, networkError: Bool) {
        let message = handleErrorMessage(error: error, networkError: networkError)
        contentIsLoading = false
        self.presenterDelegate?.shouldShowError(withMessage: message)
    }
    
    // MARK: CellSetup
    
    func numberOfSections() -> Int {
        return 1
    }
    
    func numberOfRows(inSection section: Int) -> Int {
        return characters.count
    }
    
}
