//
//  ItemCellView.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 22/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

struct ItemCellDto {
    var isFavorite: Bool = false
    var currentItemId = 0
    var image = ""
    var title = ""
}

protocol ItemFavoriteDelegate: class {
    func didSelectedFavorite(withId id: Int, isSelected: Bool)
}

class ItemCellView: UIView {

    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    private weak var favoriteDelegate: ItemFavoriteDelegate?
    private var currentItemId = 0
    private var isFavorite = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setAutomationIds()
    }
    
    private func setAutomationIds() {
        let prefix = String(describing: ItemCellView.self)
        favoriteButton.accessibilityIdentifier = prefix + ".favoriteButton"
        itemImageView.accessibilityIdentifier = prefix + ".itemImageView"
        itemImageView.accessibilityIdentifier = prefix + ".titleLabel"
    }
    
    func fill(dto: ItemCellDto, favoriteDelegate: ItemFavoriteDelegate?, shouldHideFavoriteIcon: Bool = false) {
        self.favoriteDelegate = favoriteDelegate
        self.currentItemId = dto.currentItemId
        itemImageView.downloadImage(url: dto.image)
        titleLabel.text = dto.title
        favoriteButton.isHidden = shouldHideFavoriteIcon
        let favoriteAsset: ImageAsset = dto.isFavorite ? .favoriteOn : .favoriteOff
        favoriteButton.setImageForAllStates(favoriteAsset)
        isFavorite = dto.isFavorite
    }

    @IBAction func favoriteAction(_ sender: Any) {
        self.favoriteDelegate?.didSelectedFavorite(withId: currentItemId, isSelected: isFavorite)
    }
   
}
