//
//  ItemCollectionViewCell.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 22/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

class ItemCollectionViewCell: UICollectionViewCell {
    
    let view: ItemCellView = ItemCellView.loadFromNib()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.fillWithSubview(subview: view)
    }
    
    func fill(dto: ItemCellDto, favoriteDelegate: ItemFavoriteDelegate?, shouldHideFavoriteIcon: Bool = false) {
        view.fill(dto: dto, favoriteDelegate: favoriteDelegate, shouldHideFavoriteIcon: shouldHideFavoriteIcon)
    }
    
    func cancelDownloadTask() {
        view.itemImageView.kf.cancelDownloadTask()
    }
    
}
