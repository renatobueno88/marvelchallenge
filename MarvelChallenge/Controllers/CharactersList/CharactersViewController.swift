//
//  CharactersViewController.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 22/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

protocol ItemsCollectionProtocol {
    var collectionView: UICollectionView! { get }
}

class CharactersViewController: UIViewController, CollectionViewProtocols, UISearchBarDelegate, ListPresenterDelegate, ItemFavoriteDelegate, DefaultItemCellSetupProtocol {
    
    @IBOutlet weak var collectionView: UICollectionView!
    private var pullToRefresh = UIRefreshControl()
    lazy var presenter = CharacterListPresenter(presenterDelegate: self)
    private var page: Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.title = presenter.charactersListTitle
        setupPulltoRefresh()
        addSearchController()
        loadContent()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter.loadFavorites()
        if !presenter.favoriteCharacters.isEmpty {
            reloadCollectionView()
        }
    }
    
    func loadContent(page: Int? = nil, refreshContent: Bool = false) {
        presenter.loadContent(page: page, refreshContent: refreshContent)
    }
    
    // MARK: PullToRefresh
    
    private func setupPulltoRefresh() {
        collectionView.refreshControl = pullToRefresh
        pullToRefresh.addTarget(self, action: #selector(CharactersViewController.reloadContent), for: .valueChanged)
    }
    
    @objc func reloadContent() {
        loadContent(refreshContent: true)
    }
    
    // MARK: DefaultItemCellSetupProtocol
    
    func createCharacterCell(collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = createCharacterCell(collectionView: collectionView, cellForItemAt: indexPath, dto: presenter.getCharacterItemDto(atIndex: indexPath), favoriteDelegate: self)
        return cell
    }
    
    // MARK: CollectionViewProtocols
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return createCharacterCell(collectionView: collectionView, cellForItemAt: indexPath)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return presenter.numberOfSections()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.numberOfRows(inSection: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let character = presenter.presentSelectedCharacter(atIndex: indexPath)
        self.showCharacterDetail(character: character, characterId: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.collectionView?.calculateSizeCell(numberOfCells: 2, cellSpace: nil) ?? CGSize(width: 0, height: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell = cell as? ItemCollectionViewCell {
            cell.cancelDownloadTask()
        }
    }

    func reloadCollectionView() {
        DispatchQueue.main.async {
            self.pullToRefresh.endRefreshing()
            self.collectionView.reloadData()
        }
    }
    
    // MARK: ListPresenterDelegate
    
    func didFinishRequest(currentPage page: Int) {
        self.page = page
        reloadCollectionView()
    }
    
    func shouldShowError(withMessage message: String) {
        showAlertWithTryAgain(message: message)
    }
    
    private func showAlertWithTryAgain(message: String) {
        self.showDefaultSystemAlert(message: message) { [unowned self] _ in
            self.loadContent()
        }
    }
    
    func presentEmptyResults() {
        let message = "Ops! Parece que não houveram resultados"
        showAlertWithTryAgain(message: message)
    }
    
    // MARK: INFINITE SCROLLING
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        if offsetY > contentHeight - scrollView.frame.size.height && !presenter.contentIsLoading, let page = page {
            self.loadContent(page: page)
        }
    }
    
    // MARK: SearchController
    
    func addSearchController() {
        let search = UISearchController(searchResultsController: nil)
        search.searchBar.delegate = self
        navigationItem.hidesSearchBarWhenScrolling = false
        search.searchBar.placeholder = "Buscar personagem"
        self.definesPresentationContext = true
        self.navigationItem.searchController = search
    }
    
    // MARK: UISearchBarDelegate
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
        self.makeSearch(withSearchbar: searchBar)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    
    // MARK: Search
    
    private func makeSearch(withSearchbar searchBar: UISearchBar) {
        guard let text = searchBar.text, text.count >= 3 else {
            return
        }
        self.showSearch(searchTerm: text)
    }
    
    // MARK: ItemFavoriteDelegate
    
    func didSelectedFavorite(withId id: Int, isSelected: Bool) {
        guard let row = presenter.getIndexFromFavorite(withId: id) else {
            return
        }
        if isSelected {
            presenter.removeItemAsFavorite(withId: id)
        } else {
            presenter.saveItemAsFavorite(withId: id)
        }
        let indexPath = IndexPath(row: row, section: 0)
        reloadAtIndex(indexPath: indexPath)
    }
    
    func reloadAtIndex(indexPath: IndexPath) {
        DispatchQueue.main.async {
            guard self.collectionView.hasRowAtIndexPath(indexPath: indexPath) else {
                return
            }
            self.collectionView.reloadItems(at: [indexPath])
        }
    }
    
    // MARK: NAVIGATION
    
    func showSearch(searchTerm: String) {
        guard let controller = SearchControllerBuilder(searchTerm: searchTerm).build() else {
            return
        }
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func showCharacterDetail(character: MarvelObject?, characterId: Int?) {
        guard let controller = DetailControllerBuilder(selectedCharacter: character, characterId: characterId).build() else {
            return
        }
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
