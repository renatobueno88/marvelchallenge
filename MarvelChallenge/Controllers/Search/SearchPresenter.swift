//
//  SearchPresenter.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 23/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

protocol SearchPresenterDelegate: ListPresenterDelegate { }

class SearchPresenter: SearchInteractorDelegate, AddToFavoriteProtocol, PagingHandlerProtocol, ErrorMessagingProtocol {
    
    private weak var presenterDelegate: SearchPresenterDelegate?
    lazy var interactor: SearchCharacterProtocol = SearchCharactersInteractor(interactorDelegate: self)
    private var localServiceType: FavoriteLocalProtocol.Type
    var localService: FavoriteLocalProtocol {
        return localServiceType.init(favoriteKey: "UserFavorites")
    }
    var shouldStopPaging: Bool = false
    var favoriteCharacters = [Int: Bool]()
    
    var characters = [MarvelObject]()
    var resultsData = DataObject()
    var totalPages: Int?
    var contentIsLoading = false
    
    var searchTerm = ""
    var searchTermType: SearchTermType = .name
    
    init(presenterDelegate: SearchPresenterDelegate?, localServiceType: FavoriteLocalProtocol.Type = FavoriteLocalService.self) {
        self.presenterDelegate = presenterDelegate
        self.localServiceType = localServiceType
    }
    
    func makeSearch(page: Int? = nil, limit: Int = 20) {
        let pageOffset = handlePaging(page: page, totalPages: totalPages)
        if shouldStopPaging {
            contentIsLoading = false
            return
        }
        if searchTerm.isEmpty {
            self.presenterDelegate?.presentEmptyResults()
            contentIsLoading = false
            return
        }
        contentIsLoading = true
        interactor.searchCharacters(withText: searchTerm, limit: limit, page: pageOffset, searchTermType: searchTermType)
    }
    
    func getCharacterItemDto(atIndex indexPath: IndexPath) -> ItemCellDto {
        guard !characters.isEmpty else {
            return ItemCellDto()
        }
        let character = characters[indexPath.row]
        let itemIsFavorite = checkItemIsFavorite(withId: character)
        return ItemCellDto(isFavorite: itemIsFavorite, currentItemId: character.id, image: character.thumbnail?.thumbnailPath ?? "", title: character.titleName)
    }
    
    var getCurrentSearchTerm: String {
        return searchTerm
    }
    
    func presentSelectedCharacter(atIndex indexPath: IndexPath) -> MarvelObject {
        return characters[indexPath.row]
    }
    
    func appendRemovingDuplicates(results: [MarvelObject]) {
        results.forEach { (character) in
            guard !characters.contains(where: { $0.id == character.id }) else {
                return
            }
            characters.append(character)
        }
    }
    
    // MARK: SearchInteractorDelegate
    
    func didFinishRequest(withEntity entity: DataObject) {
        self.resultsData = entity
        self.totalPages = entity.total
        appendRemovingDuplicates(results: entity.results)
        
        if entity.count == 0 {
            self.presenterDelegate?.presentEmptyResults()
        } else {
            self.presenterDelegate?.didFinishRequest(currentPage: entity.offset)
        }
        contentIsLoading = false
    }
    
    func shouldShowError(error: Error?, networkError: Bool) {
        let message = handleErrorMessage(error: error, networkError: networkError)
        contentIsLoading = false
        self.presenterDelegate?.shouldShowError(withMessage: message)
    }
    
    // MARK: CellSetup
    
    func numberOfSections() -> Int {
        return 1
    }
    
    func numberOfRows(inSection section: Int) -> Int {
        return characters.count
    }
    
}
