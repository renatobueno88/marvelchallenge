//
//  SearchViewController.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 23/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, CollectionViewProtocols, SearchPresenterDelegate, ItemFavoriteDelegate, DefaultItemCellSetupProtocol {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var page: Int?
    lazy var presenter = SearchPresenter(presenterDelegate: self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        self.navigationController?.navigationBar.prefersLargeTitles = true
        activityIndicator.startAnimating()
        activityIndicator.hidesWhenStopped = true
        self.title = presenter.getCurrentSearchTerm
        self.loadSearch()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter.loadFavorites()
        if !presenter.favoriteCharacters.isEmpty {
            reloadCollectionView()
        }
    }
    
    func loadSearch(page: Int? = nil) {
        presenter.makeSearch(page: page)
    }
    
    // MARK: DefaultItemCellSetupProtocol
    
    func createCharacterCell(collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = createCharacterCell(collectionView: collectionView, cellForItemAt: indexPath, dto: presenter.getCharacterItemDto(atIndex: indexPath), favoriteDelegate: self)
        return cell
    }
    
    // MARK: CollectionViewProtocols
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return createCharacterCell(collectionView: collectionView, cellForItemAt: indexPath)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return presenter.numberOfSections()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.numberOfRows(inSection: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let character = presenter.presentSelectedCharacter(atIndex: indexPath)
        self.showCharacterDetail(character: character)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.collectionView?.calculateSizeCell(numberOfCells: 2, cellSpace: nil) ?? CGSize(width: 0, height: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell = cell as? ItemCollectionViewCell {
            cell.cancelDownloadTask()
        }
    }
    
    func reloadCollectionView() {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
            self.collectionView.reloadData()
        }
    }
    
    // MARK: SearchPresenterDelegate
    
    func didFinishRequest(currentPage page: Int) {
        self.page = page
        reloadCollectionView()
    }
    
    func shouldShowError(withMessage message: String) {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
        }
        self.showDefaultSystemAlert(message: message) { [unowned self] _ in
            self.loadSearch()
        }
    }
    
    func presentEmptyResults() {
        let message = "Ops! Parece que não houveram resultados"
        self.showDefaultSystemAlert(message: message, actionTitle: "Buscar novamente", cancelTitle: "") { [unowned self] _ in
            self.activityIndicator.stopAnimating()
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    // MARK: INFINITE SCROLLING
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        if offsetY > contentHeight - scrollView.frame.size.height && !presenter.contentIsLoading, let page = page {
            self.loadSearch(page: page)
        }
    }

    // MARK: ItemFavoriteDelegate
    
    func didSelectedFavorite(withId id: Int, isSelected: Bool) {
        if isSelected {
            presenter.removeItemAsFavorite(withId: id)
        } else {
            presenter.saveItemAsFavorite(withId: id)
        }
        guard let row = presenter.getIndexFromFavorite(withId: id) else {
            return
        }
        let indexPath = IndexPath(row: row, section: 0)
        DispatchQueue.main.async {
            guard self.collectionView.hasRowAtIndexPath(indexPath: indexPath) else {
                return
            }
            self.collectionView.reloadItems(at: [indexPath])
        }
    }
    
    // MARK: Navigation
    
    func showCharacterDetail(character: MarvelObject) {
        guard let controller = DetailControllerBuilder(selectedCharacter: character).build() else {
            return
        }
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
