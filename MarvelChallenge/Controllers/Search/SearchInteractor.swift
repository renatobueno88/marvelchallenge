//
//  SearchInteractor.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 23/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

protocol SearchInteractorDelegate: ListInteractorDelegate { }

protocol SearchCharacterProtocol {
    func searchCharacters(withText text: String, limit: Int, page: Int, searchTermType: SearchTermType)
}

class SearchCharactersInteractor: SearchCharacterProtocol {
    
    private var searchServiceType: SearchRequestService.Type
    private var searchRequest: SearchRequestService {
        return searchServiceType.init()
    }
    private weak var interactorDelegate: SearchInteractorDelegate?
    
    init(searchServiceType: SearchRequestService.Type = SearchRequest.self, interactorDelegate: SearchInteractorDelegate?) {
        self.interactorDelegate = interactorDelegate
        self.searchServiceType = searchServiceType
    }
    
    // MARK: SearchCharacterProtocol
    
    func searchCharacters(withText text: String, limit: Int, page: Int, searchTermType: SearchTermType) {
        searchRequest.fetchCharactersFromSearch(searchTerm: text, searchTermType: searchTermType, paging: page, limit: limit) { (resultsData, error) in
            guard let resultsData = resultsData, let data = resultsData.data, error == nil else {
                let hasNetworkError = !(Reachability.isConnectedToNetwork())
                self.interactorDelegate?.shouldShowError(error: error, networkError: hasNetworkError)
                return
            }
            self.interactorDelegate?.didFinishRequest(withEntity: data)
        }
    }
    
}
