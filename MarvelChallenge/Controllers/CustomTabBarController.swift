//
//  CustomTabBarController.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 22/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

enum CustomTabBarItems: Int, CaseIterable {
    case list
    case favorites
    
    var title: String {
        switch self {
        case .list:
            return "Personagens"
        case .favorites:
            return "Favoritos"
        }
    }
    
    var icon: UIImage? {
        switch self {
        case .list:
            return UIImage(named: ImageAsset.homeIcon.rawValue)
        case .favorites:
            return UIImage(named: ImageAsset.favoriteIcon.rawValue)
        }
    }
}

class CustomTabBarController: UITabBarController {

    static var sharedInstance: CustomTabBarController? {
        return UIApplication.shared.keyWindow?.rootViewController as? CustomTabBarController
    }
    
    var listInstance: CharactersViewController? {
        var listInstance: CharactersViewController?
        viewControllers?.forEach { navigation in
            if let characterController = navigation.children.first(where: { $0 is CharactersViewController }) as? CharactersViewController {
                listInstance = characterController
                return
            }
        }
        return listInstance
    }
    
    var favoritesInstance: FavoriteItemsViewController? {
        var favoritesInstance: FavoriteItemsViewController?
        viewControllers?.forEach { navigation in
            if let favController = navigation.children.first(where: { $0 is FavoriteItemsViewController }) as? FavoriteItemsViewController {
                favoritesInstance = favController
                return
            }
        }
        return favoritesInstance
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAppearance()
    }
 
    func switchTabBar(selected: CustomTabBarItems) {
        self.selectedIndex = selected.rawValue
    }
    
    func setupAppearance() {
        let icons = CustomTabBarItems.allCases
        for (index, icon) in icons.enumerated() {
            if let tabBarItems = self.tabBar.items {
                let tabBarIcon = tabBarItems[index]
                tabBarIcon.image = icon.icon
                tabBarIcon.title = icon.title
                tabBarIcon.selectedImage = icon.icon
            }
        }
    }
    

}
