//
//  CharacterDetailPresenter.swift
//  MarvelChallenge
//
//  Created by Renato de Souza Bueno on 24/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

enum DetailCellType: Int, CaseIterable {
    case image
    case description
    case comics
    case series
    
    var cellHeight: CGFloat {
        switch self {
        case .image, .description:
            return UITableView.automaticDimension
        case .comics, .series:
            return 270
        }
    }
}

protocol CharacterDetailPresenterDelegate: class {
    func didFinishRequest(inCellType: DetailCellType?)
    func shouldShowError(errorMessage: String)
}

class CharacterDetailPresenter: CellTypeSetupProtocol, AddToFavoriteProtocol, TransporterProtocol, CharacterDetailInteractorDelegate, ErrorMessagingProtocol {
    
    var character = MarvelObject()
    private weak var presenterDelegate: CharacterDetailPresenterDelegate?
    private var localServiceType: FavoriteLocalProtocol.Type
    var localService: FavoriteLocalProtocol {
        return localServiceType.init(favoriteKey: "UserFavorites")
    }
    lazy var interactor: CharacterComicsSeriesProtocol = CharacterDetailInteractor(interactorDelegate: self)
    var marvelResultData = DataObject()
    var favoriteCharacters = [Int: Bool]()
    var totalPages: Int?
    var contentIsLoading = false
    var shouldStopPaging: Bool = false
    var characters: [MarvelObject] = [MarvelObject]()
    private var comicsResults = [MarvelObject]()
    private var seriesResults = [MarvelObject]()
    private var characterId: Int?
    
    var seriesContentIsLoading = false
    
    init(localServiceType: FavoriteLocalProtocol.Type = FavoriteLocalService.self, presenterDelegate: CharacterDetailPresenterDelegate?) {
        self.localServiceType = localServiceType
        self.presenterDelegate = presenterDelegate
    }
    
    var shouldMakeComicsRequest: Bool {
        return !(character.comics?.items?.isEmpty ?? false)
    }
    
    var shouldMakeSeriesRequest: Bool {
        return !(character.series?.items?.isEmpty ?? false)
    }
    
    func fetchComicSeriesIfAvailable(forceRequest: Bool = false) {
        if shouldMakeComicsRequest && shouldMakeSeriesRequest || forceRequest {
            let group = DispatchGroup()
            group.enter()
            MarvelProductsType.allCases.forEach { (type) in
                fetchProducts(type: type)
            }
            group.leave()
            return
        } else if shouldMakeComicsRequest {
            fetchProducts(type: .comics)
        } else if shouldMakeSeriesRequest {
            fetchProducts(type: .series)
        } else {
            populateCells()
        }
    }
    
    func fetchCharacterDetail() {
        guard let id = characterId else {
            return
        }
        contentIsLoading = true
        interactor.fetchCharacterDetail(withId: id)
    }
    
    func fetchProducts(type: MarvelProductsType, limit: Int = 20) {
        contentIsLoading = true
        interactor.fetchComicsSeries(withProductType: type, limit: limit, page: 0, characterId: character.id)
    }
    
    // MARK: TransporterProtocol
    
    func transporter(object: Transporter<Any>) {
        if let character = object.data as? MarvelObject {
            self.character = character
            if !characters.contains(where: { $0.id == character.id }) {
                characters.append(character)
            }
            self.fetchComicSeriesIfAvailable()
            self.populateCells()
        } else if let id = object.data as? Int {
            self.characterId = id
            self.fetchCharacterDetail()
        }
    }
    
    func populateCells() {
        var cells: [CellType] = []
        if shouldShowCell(cellTypes: .image) {
            cells.append(.image)
        }
        if shouldShowCell(cellTypes: .description) {
            cells.append(.description)
        }
        if shouldShowCell(cellTypes: .comics) {
            cells.append(.comics)
        }
        if shouldShowCell(cellTypes: .series) {
            cells.append(.series)
        }
        cellTypes = cells
    }
    
    func shouldShowCell(cellTypes: CellType) -> Bool {
        switch cellTypes {
        case .image:
            return !(character.thumbnail?.thumbnailPath.isEmpty ?? false)
        case .description:
            return !(character.description?.isEmpty ?? false)
        case .comics:
            return character.comics != nil
        case .series:
            return character.series != nil
        }
    }
    
    func getCellType(FromIndex index: IndexPath) -> DetailCellType {
        return cellTypes[index.row]
    }
    
    var getCharacterName: String {
        return character.titleName
    }
    
    var getImagePath: String {
        return character.thumbnail?.thumbnailPath ?? ""
    }
    
    var getCharacterDescription: String {
        return character.description ?? ""
    }
    
    func getComics() -> MarvelProductsTransporter {
        let objects = comicsResults.compactMap({ ComicsSeriesCellDto(title: $0.titleName, imagePath: $0.thumbnail?.thumbnailPath ?? "", id: $0.id) })
        let title = comicsResults.isEmpty ? "" : "Quadrinhos"
        return (objects: objects, title: title)
    }
    
    func getSeries() -> MarvelProductsTransporter {
        let objects = seriesResults.compactMap({ ComicsSeriesCellDto(title: $0.titleName, imagePath: $0.thumbnail?.thumbnailPath ?? "", id: $0.id) })
        let title = seriesResults.isEmpty ? "" : "Séries"
        return (objects: objects, title: title)
    }
    
    func getCellTypeFromProductType(type: MarvelProductsType) -> CellType {
        return type == .comics ? .comics : .series
    }
    
    func productIsFavorite() -> Bool {
        return checkItemIsFavorite(withId: character)
    }
    
    // MARK: CellTypeSetupProtocol
    
    typealias CellType = DetailCellType
    var cellTypes: [DetailCellType] = []
    
    func numberOfSections() -> Int {
        return 1
    }
    
    func numberOfRows(inSection section: Int) -> Int {
        return cellTypes.count
    }
    
    func estimatedHeightForCell(indexPath: IndexPath) -> CGFloat {
        guard !cellTypes.isEmpty else {
            return .leastNonzeroMagnitude
        }
        let cellType = cellTypes[indexPath.row]
        return shouldShowCell(cellTypes: cellType) ? cellType.cellHeight : .leastNonzeroMagnitude
    }
    
    // MARK: CharacterDetailInteractorDelegate
    
    func didFinishRequest(withEntity entity: DataObject, type: MarvelProductsType) {
        self.marvelResultData = entity
        
        if type == .comics {
            comicsResults.append(contentsOf: entity.results)
        } else {
            seriesResults.append(contentsOf: entity.results)
        }
        let cellType = getCellTypeFromProductType(type: type)
        presenterDelegate?.didFinishRequest(inCellType: cellType)
        contentIsLoading = false
    }
    
    func shouldHideProduct(withType type: MarvelProductsType) {
        contentIsLoading = false
        let cellType = getCellTypeFromProductType(type: type)
        presenterDelegate?.didFinishRequest(inCellType: cellType)
    }
    
    func didFinishDetailRequest(withEntity entity: [MarvelObject]) {
        guard let character = entity.first else {
            return
        }
        self.character = character
        self.fetchComicSeriesIfAvailable(forceRequest: true)
        self.populateCells()
        presenterDelegate?.didFinishRequest(inCellType: nil)
    }
    
    func detailRequestShouldShowError(error: Error?, networkError: Bool) {
        let message = self.handleErrorMessage(error: error, networkError: networkError)
        presenterDelegate?.shouldShowError(errorMessage: message)
    }

}
