//
//  ComicsSeriesTableViewCell.swift
//  MarvelChallenge
//
//  Created by Renato de Souza Bueno on 24/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

class ComicsSeriesTableViewCell: UITableViewCell, TransporterProtocol {

    let view: ComicsSeriesCellView = ComicsSeriesCellView.loadFromNib()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.fillWithSubview(subview: view)
    }

    func transporter(object: Transporter<Any>) {
        view.transporter(object: object)
    }

}
