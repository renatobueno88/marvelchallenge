//
//  HeaderViewCollectionReusableView.swift
//  MarvelChallenge
//
//  Created by Renato de Souza Bueno on 24/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

class HeaderViewCollectionReusableView: UICollectionReusableView {
    
    let view: DescriptionCellView = DescriptionCellView.loadFromNib()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.fillWithSubview(subview: view)
    }
    
    func fill(withText text: String, font: UIFont = .boldSystemFont(ofSize: 16)) {
        view.fill(descriptionText: text, font: font)
    }
        
}
