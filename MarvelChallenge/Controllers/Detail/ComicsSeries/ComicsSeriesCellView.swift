//
//  ComicsSeriesCellView.swift
//  MarvelChallenge
//
//  Created by Renato de Souza Bueno on 24/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

class ComicsSeriesCellView: UIView, TransporterProtocol, CollectionViewProtocols, DefaultItemCellSetupProtocol {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    lazy var presenter = ComicsSeriesCellPresenter()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.registerCell(identifier: String(describing: ItemCollectionViewCell.self))
        setAutomationIds()
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
    }
    
    private func setAutomationIds() {
        let prefix = String(describing: ComicsSeriesCellView.self)
        collectionView.accessibilityIdentifier = prefix + ".collectionView"
        headerLabel.accessibilityIdentifier = prefix + ".headerLabel"
        activityIndicator.accessibilityIdentifier = prefix + "activityIndicator"
    }
    
    // MARK: TransporterProtocol
    
    func transporter(object: Transporter<Any>) {
        presenter.transporter(object: object)
        let title = presenter.headerTitle
        DispatchQueue.main.async {
            self.headerLabel.text = title
            self.collectionView.reloadData()
            if title.isEmpty {
                self.activityIndicator.stopAnimating()
            }
        }
    }
    
    // MARK: DefaultItemCellSetupProtocol
    
    func createCharacterCell(collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        activityIndicator.stopAnimating()
        let cell = createCharacterCell(collectionView: collectionView, cellForItemAt: indexPath, dto: presenter.getCharacterItemDto(atIndex: indexPath), shouldHideFavoriteIcon: true)
        return cell
    }
    
    // MARK: CollectionViewProtocols
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return createCharacterCell(collectionView: collectionView, cellForItemAt: indexPath)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return presenter.numberOfSections()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.numberOfRows(inSection: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.collectionView?.calculateSizeCell(numberOfCells: 2.5, cellSpace: 4) ?? CGSize(width: 0, height: 0)
    }

}
