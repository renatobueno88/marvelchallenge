//
//  ComicsSeriesCellPresenter.swift
//  MarvelChallenge
//
//  Created by Renato de Souza Bueno on 24/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

struct ComicsSeriesCellDto {
    var title = ""
    var imagePath = ""
    var id = 0
}

typealias MarvelProductsTransporter = (objects: [ComicsSeriesCellDto], title: String)

class ComicsSeriesCellPresenter: TransporterProtocol {
  
    private(set) var comicsSeries = [ComicsSeriesCellDto]()
    private(set) var headerTitle = ""
    
    init() {
        
    }
    
    func transporter(object: Transporter<Any>) {
        guard let comicsSeries = object.data as? MarvelProductsTransporter else {
            return
        }
        self.comicsSeries = comicsSeries.objects
        self.headerTitle = comicsSeries.title
    }
    
    func getCharacterItemDto(atIndex indexPath: IndexPath) -> ItemCellDto {
        guard !comicsSeries.isEmpty else {
            return ItemCellDto()
        }
        let product = comicsSeries[indexPath.row]
        return ItemCellDto(isFavorite: false, currentItemId: product.id, image: product.imagePath, title: product.title)
    }
    
    func numberOfRows(inSection section: Int) -> Int {
        return comicsSeries.count
    }
    
    func numberOfSections() -> Int {
        return 1
    }
    
}
