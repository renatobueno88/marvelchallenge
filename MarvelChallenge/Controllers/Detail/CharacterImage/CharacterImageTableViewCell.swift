//
//  CharacterImageTableViewCell.swift
//  MarvelChallenge
//
//  Created by Renato de Souza Bueno on 24/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

class CharacterImageTableViewCell: UITableViewCell {
    
    let view: CharacterImageCellView = CharacterImageCellView.loadFromNib()

    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.fillWithSubview(subview: view)
    }

    func fill(withImagePath imagePath: String)  {
        view.fill(withImagePath: imagePath)
    }
}
