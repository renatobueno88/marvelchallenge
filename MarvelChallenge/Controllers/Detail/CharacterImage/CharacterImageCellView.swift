//
//  CharacterImageCellView.swift
//  MarvelChallenge
//
//  Created by Renato de Souza Bueno on 24/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

class CharacterImageCellView: UIView {

    @IBOutlet weak var characterImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setAutomationIds()
    }
    
    private func setAutomationIds() {
        let prefix = String(describing: CharacterImageCellView.self)
        characterImageView.accessibilityIdentifier = prefix + ".characterImageView"
    }
    
    func fill(withImagePath imagePath: String) {
        characterImageView.downloadImage(url: imagePath)
    }
}
