//
//  CharacterDetailInteractor.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 24/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

protocol CharacterDetailInteractorDelegate: class {
    func didFinishRequest(withEntity entity: DataObject, type: MarvelProductsType)
    func didFinishDetailRequest(withEntity entity: [MarvelObject])
    func detailRequestShouldShowError(error: Error?, networkError: Bool)
    func shouldHideProduct(withType type: MarvelProductsType)
}

protocol CharacterComicsSeriesProtocol {
    func fetchComicsSeries(withProductType type: MarvelProductsType, limit: Int, page: Int, characterId: Int)
    func fetchCharacterDetail(withId id: Int)
}

class CharacterDetailInteractor: CharacterComicsSeriesProtocol {
    
    private var characterDetailRequestType: SingleCharacterRequestService.Type
    private var characterDetailRequest: SingleCharacterRequestService {
        return characterDetailRequestType.init()
    }
    private var comicsSeriesRequestType: ComicsSeriesService.Type
    private var comicsSeriesRequest: ComicsSeriesService {
        return comicsSeriesRequestType.init()
    }
    private weak var interactorDelegate: CharacterDetailInteractorDelegate?
    
    init(comicsSeriesRequestType: ComicsSeriesService.Type = ComicsSeriesRequest.self, characterDetailRequestType: SingleCharacterRequestService.Type = SingleCharacterRequest.self, interactorDelegate: CharacterDetailInteractorDelegate?) {
        self.interactorDelegate = interactorDelegate
        self.comicsSeriesRequestType = comicsSeriesRequestType
        self.characterDetailRequestType = characterDetailRequestType
    }
    
    // MARK: CharacterComicsSeriesProtocol
    
    func fetchCharacterDetail(withId id: Int) {
        characterDetailRequest.fetchSingleCharacter(characterId: id) { (resultsData, error) in
            guard let resultsData = resultsData, let data = resultsData.data, error == nil else {
                let hasNetworkError = !(Reachability.isConnectedToNetwork())
                self.interactorDelegate?.detailRequestShouldShowError(error: error, networkError: hasNetworkError)
                return
            }
            self.interactorDelegate?.didFinishDetailRequest(withEntity: data.results)
        }
    }
    
    func fetchComicsSeries(withProductType type: MarvelProductsType, limit: Int, page: Int, characterId: Int) {
        comicsSeriesRequest.fetchItemsFromCharacter(characterId: characterId, marvelProductsType: type, paging: page, limit: limit) { (resultsData, error) in
            guard let resultsData = resultsData, let data = resultsData.data, error == nil else {
                self.interactorDelegate?.shouldHideProduct(withType: type)
                return
            }
            self.interactorDelegate?.didFinishRequest(withEntity: data, type: type)
        }
    }
    
}
