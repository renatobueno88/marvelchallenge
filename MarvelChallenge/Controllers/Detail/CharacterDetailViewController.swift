//
//  CharacterDetailViewController.swift
//  MarvelChallenge
//
//  Created by Renato de Souza Bueno on 24/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

public typealias CollectionViewProtocols = UICollectionViewDelegate & UICollectionViewDataSource & UICollectionViewDelegateFlowLayout

public typealias TableViewDelegates = UITableViewDelegate & UITableViewDataSource

class CharacterDetailViewController: UIViewController, TableViewDelegates, TransporterProtocol, CharacterDetailPresenterDelegate {

    @IBOutlet weak var tableView: UITableView!
    lazy var presenter = CharacterDetailPresenter(presenterDelegate: self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        self.title = presenter.getCharacterName
        presenter.loadFavorites()
        setupFavoriteIcon()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupFavoriteIcon()
    }
    
    func setupFavoriteIcon() {
        self.navigationItem.rightBarButtonItem = setupFavoriteOnNavigation(isFavorite: presenter.productIsFavorite())
    }
    
    override func addToFavoriteAction() {
        let id = presenter.character.id
        let isSelected = presenter.productIsFavorite()
        if isSelected {
            presenter.removeItemAsFavorite(withId: id)
        } else {
            presenter.saveItemAsFavorite(withId: id)
        }
        setupFavoriteIcon()
    }
    
    //MARK: TransporterProtocol
    
    func transporter(object: Transporter<Any>) {
        presenter.transporter(object: object)
    }

    func createCharacterImageCell(tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CharacterImageTableViewCell = CharacterImageTableViewCell.createCell(tableView: tableView, indexPath: indexPath)
        cell.fill(withImagePath: presenter.getImagePath)
        return cell
    }
    
    func createDescriptionCell(tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell  {
        let cell: DescriptionTableViewCell = DescriptionTableViewCell.createCell(tableView: tableView, indexPath: indexPath)
        cell.fill(descriptionText: presenter.getCharacterDescription)
        return cell
    }
    
    func createComicsSeriesCell(tableView: UITableView, cellForRowAt indexPath: IndexPath, type: DetailCellType) -> UITableViewCell {
        let cell: ComicsSeriesTableViewCell = ComicsSeriesTableViewCell.createCell(tableView: tableView, indexPath: indexPath)
        let dto = type == .comics ? presenter.getComics() : presenter.getSeries()
        cell.transporter(object: Transporter(data: dto))
        return cell
    }
    
    // MARK: TableViewDelegates
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let type = presenter.getCellType(FromIndex: indexPath)
        switch type {
        case .image:
            return createCharacterImageCell(tableView: tableView, cellForRowAt: indexPath)
        case .description:
            return createDescriptionCell(tableView: tableView, cellForRowAt: indexPath)
        case .comics, .series:
            return createComicsSeriesCell(tableView: tableView, cellForRowAt: indexPath, type: type)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return presenter.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRows(inSection: section)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return presenter.estimatedHeightForCell(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return presenter.estimatedHeightForCell(indexPath: indexPath)
    }
    
    // MARK: CharacterDetailPresenterDelegate
    
    func shouldShowError(errorMessage: String) {
        self.showDefaultSystemAlert(message: errorMessage) { [unowned self] _ in
            self.presenter.fetchCharacterDetail()
        }
    }
    
    func didFinishRequest(inCellType: DetailCellType?) {
        guard let cellType = inCellType else {
            DispatchQueue.main.async {
                self.title = self.presenter.getCharacterName
                self.tableView.reloadData()
            }
            return
        }
        guard let row = presenter.cellTypes.firstIndex(of: cellType) else {
            return
        }
        let indexPath = IndexPath(row: row, section: 0)
        DispatchQueue.main.async {
            if self.tableView.hasRowAtIndexPath(indexPath: indexPath) {
                self.tableView.reloadRows(at: [indexPath], with: .fade)
            }
        }
    }
    
}
