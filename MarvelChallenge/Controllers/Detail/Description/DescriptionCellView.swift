//
//  DescriptionCellView.swift
//  MarvelChallenge
//
//  Created by Renato de Souza Bueno on 24/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

class DescriptionCellView: UIView {

    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setAutomationIds()
    }
    
    private func setAutomationIds() {
        let prefix = String(describing: DescriptionCellView.self)
        descriptionLabel.accessibilityIdentifier = prefix + ".descriptionLabel"
    }
    
    func fill(descriptionText text: String, font: UIFont = .systemFont(ofSize: 16)) {
        descriptionLabel.font = font
        descriptionLabel.text = text
    }

}
