//
//  DescriptionTableViewCell.swift
//  MarvelChallenge
//
//  Created by Renato de Souza Bueno on 24/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

class DescriptionTableViewCell: UITableViewCell {
    
    let view: DescriptionCellView = DescriptionCellView.loadFromNib()

    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.fillWithSubview(subview: view)
    }

    func fill(descriptionText text: String, font: UIFont = .systemFont(ofSize: 16))  {
        view.fill(descriptionText: text, font: font)
    }
    
}
