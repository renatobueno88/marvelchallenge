//
//  MarvelItems.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 22/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

struct MarvelItems: Codable {
    
    var resourceURI: String = ""
    var name: String = ""
   
    enum CodingKeys: String, CodingKey {
        case resourceURI
        case name
    }
}
