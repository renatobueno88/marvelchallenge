//
//  Series.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 22/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

struct Series: Codable, MarvelProductsEntityProtocol {
    
    var available: Int?
    var collectionURI: String?
    var items: [MarvelItems]?

    enum CodingKeys: String, CodingKey {
        case available
        case collectionURI
        case items
    }
}
