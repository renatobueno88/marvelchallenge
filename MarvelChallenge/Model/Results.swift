//
//  Results.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 22/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

struct MarvelResults: Codable {
    
    var id: Int = 0
    var name: String = ""
    var description: String = ""
    var thumbnail: Thumbnail?
    var comics: Comics?
    var series: Series?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case description
        case thumbnail
        case comics
        case series
    }
}

struct Thumbnail: Codable {
    
    private var path: String = ""
    private var fileExtension: String = ""
    var thumbnailPath: String {
        return path + ".\(fileExtension)"
    }
    
    private enum CodingKeys: String, CodingKey {
        case path
        case fileExtension = "extension"
    }
    
}
