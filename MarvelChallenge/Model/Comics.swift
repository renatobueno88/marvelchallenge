//
//  Comics.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 22/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

protocol MarvelProductsEntityProtocol: Codable {
    var available: Int? { get set }
    var collectionURI: String? { get set }
    var items: [MarvelItems]? { get set }
}

struct Comics: Codable, MarvelProductsEntityProtocol {
    
    var available: Int?
    var collectionURI: String?
    var items: [MarvelItems]?
    
    enum CodingKeys: String, CodingKey {
        case available
        case collectionURI
        case items
    }
    
}
