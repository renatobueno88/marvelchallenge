//
//  ResultsData.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 22/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

struct ResultObject: Codable {
    enum CodingKeys: String, CodingKey {
        case code = "code"
        case status = "status"
        case data = "data"
    }
    
    var code: Int?
    var status = ""
    var data: DataObject?
}

struct DataObject: Codable {
    enum CodingKeys: String, CodingKey {
        case offset = "offset"
        case limit = "limit"
        case total = "total"
        case count = "count"
        case results = "results"
    }
    
    var offset = 0
    var limit = 0
    var total = 0
    var count = 0
    var results = [MarvelObject]()
}

struct MarvelObject: Codable, Equatable {
    
    static func == (lhs: MarvelObject, rhs: MarvelObject) -> Bool {
        return lhs.id == rhs.id && lhs.name == rhs.name && lhs.description == rhs.description && lhs.thumbnail?.thumbnailPath ?? "" == rhs.thumbnail?.thumbnailPath ?? "" && lhs.comics?.collectionURI == rhs.comics?.collectionURI && lhs.series?.collectionURI ?? "" == rhs.series?.collectionURI ?? ""
    }
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case description = "description"
        case thumbnail = "thumbnail"
        case comics = "comics"
        case series = "series"
        case title = "title"
    }
    
    var id = 0
    private var name: String?
    private var title: String?
    var titleName: String {
        return name ?? title ?? ""
    }
    var description: String?
    var thumbnail: Thumbnail?
    var comics: Comics?
    var series: Series?
}
