//
//  CoordinatorRouter.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 22/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

protocol CoordinatorRouter {
    associatedtype Controller: UIViewController
    var storyBoardName: String { get set }
    func build() -> UIViewController?
}
