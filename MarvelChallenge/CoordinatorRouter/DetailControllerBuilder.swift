//
//  DetailControllerBuilder.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 25/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

class DetailControllerBuilder: CoordinatorRouter {
    typealias Controller = CharacterDetailViewController
    
    var storyBoardName: String = "CharacterDetail"
    
    var selectedCharacter: MarvelObject?
    var characterId: Int?
    
    init(selectedCharacter: MarvelObject?, characterId: Int? = nil) {
        self.selectedCharacter = selectedCharacter
        self.characterId = characterId
    }
    
    func build() -> UIViewController? {
        let storyboard = UIStoryboard(name: storyBoardName, bundle: Bundle.main)
        let identifier = String(describing: Controller.self)
        let detailController = storyboard.instantiateViewController(withIdentifier: identifier) as? Controller
        var transported: Any = selectedCharacter
        if let id = characterId {
            transported = id
        } else if let character = selectedCharacter {
            transported = character
        }
        detailController?.transporter(object: Transporter(data: transported))
        return detailController
    }
    
}
