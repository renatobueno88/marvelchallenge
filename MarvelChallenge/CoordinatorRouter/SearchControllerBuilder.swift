//
//  SearchControllerBuilder.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 25/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import UIKit

class CustomTabBarBuilder: CoordinatorRouter {
    typealias Controller = CustomTabBarController
    
    var storyBoardName: String = "Main"
    
    init() {
    }
    
    func build() -> UIViewController? {
        let storyboard = UIStoryboard(name: storyBoardName, bundle: Bundle.main)
        let identifier = String(describing: Controller.self)
        let tabController = storyboard.instantiateViewController(withIdentifier: identifier) as? Controller
        return tabController
    }
}


class SearchControllerBuilder: CoordinatorRouter {
    typealias Controller = SearchViewController
    
    var storyBoardName: String = "Search"
    
    var searchTerm: String = ""
    
    init(searchTerm: String) {
        self.searchTerm = searchTerm
    }
    
    func build() -> UIViewController? {
        let storyboard = UIStoryboard(name: storyBoardName, bundle: Bundle.main)
        let identifier = String(describing: Controller.self)
        let searchController = storyboard.instantiateViewController(withIdentifier: identifier) as? Controller
        searchController?.presenter.searchTerm = searchTerm
        searchController?.presenter.searchTermType = .name
        return searchController
    }
    
}
