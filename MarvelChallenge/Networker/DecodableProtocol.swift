//
//  DecodableProtocol.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 25/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

protocol DecodableProtocol {
    func decodeObject<T: Codable>(data: Data?, error: Error?, type: T.Type, completion: @escaping (T?, Error?) -> ())
}
extension DecodableProtocol {
    func decodeObject<T: Codable>(data: Data?, error: Error?, type: T.Type, completion: @escaping (T?, Error?) -> ()) {
        guard let data = data else {
            completion(nil, error)
            return
        }
        do {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            let object = try? decoder.decode(T.self, from: data)
            completion(object, nil)
        }
    }
}
