//
//  NetworkRequest.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 21/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

struct HTTPResponse {
    let result: Any?
    let error: Error?
    let rawData: Data?
}

enum HTTPMethodType {
    case get
    case post
    case put
    case delete
    
    fileprivate var typeName: String {
        switch self {
        case .get:
            return "GET"
        case .post:
            return "POST"
        case .put:
            return "PUT"
        case .delete:
            return "DELETE"
        }
    }
}

public typealias JSONDictionary = [String: Any]

class NetworkRequest: NSObject, NetworkerProtocol, URLSessionTaskDelegate, URLSessionDataDelegate {
    
    private var urlString = ""
    
    required init(urlString: String = Environment.baseUrl) {
        self.urlString = urlString
    }
    
    func makeRequest(action: ActionProtocol, method: HTTPMethodType, builder: RequestBuilder, completion: @escaping (HTTPResponse?) -> ()) {
        let baseURL = setupBaseUrl(action: action, builder: builder)
        guard let url = URL(string: baseURL) else {
            completion(nil)
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = method.typeName
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error {
                self.handleErrorResponse(error: error, completion: completion)
                return
            }
            if let data = data, let response = response {
                self.handleJsonDataResponse(response: response, data: data, completion: completion)
                return
            }
        }.resume()
    }
    
    private func setupBaseUrl(action: ActionProtocol, builder: RequestBuilder) -> String {
        var baseUrl = urlString + "\(action.actionKey)?\(defaultQueryParameters())"
        if !builder.parameters.isEmpty {
            baseUrl += builder.formatParamsToQueryString()
        }
        return baseUrl
    }
    
    private func handleErrorResponse(error: Error, completion: (_ errorResponse: HTTPResponse?) -> Void?) {
        let response = HTTPResponse(result: nil, error: error, rawData: nil)
        completion(response)
    }
    
    private func handleJsonDataResponse(response: URLResponse, data: Data, completion: (_ response: HTTPResponse?) -> Void?) {
        do {
            let deserialized = try JSONSerialization.jsonObject(with: data, options: [])
            if let json = deserialized as? JSONDictionary {
                completion(HTTPResponse(result: json, error: nil, rawData: data))
            } else if let jsonArray = deserialized as? [JSONDictionary] {
                completion(HTTPResponse(result: jsonArray, error: nil, rawData: data))
            } else {
                completion(HTTPResponse(result: [:], error: nil, rawData: data))
            }
        } catch {
            if let response = response as? HTTPURLResponse, response.statusCode == 200 {
                completion(HTTPResponse(result: nil, error: nil, rawData: data))
                return
            }
        }
    }
    
}
