//
//  MockedRequestService.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 25/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

enum MockedAction: ActionProtocol {
    
    case characters
    case comics(id: Int)
    case series(id: Int)
    case singleCharacter(id: Int)
    
    var actionKey: String {
        switch self {
        case .characters:
            return "charactersList"
        case .singleCharacter(id: _):
            return "characterDetail"
        case .comics(id: _):
            return "comics"
        case .series(id: _):
            return "series"
        }
    }
}

class MockedRequest: NetworkerProtocol, MockJsonService {
    
    required init(urlString: String) {
        
    }
    
    func makeRequest(action: ActionProtocol, method: HTTPMethodType, builder: RequestBuilder, completion: @escaping (HTTPResponse?) -> ()) {
        let data = buildObject(resource: action.actionKey)
        let response = HTTPResponse(result: nil, error: nil, rawData: data)
        completion(response)
    }
    
}

protocol MockJsonService {
    func buildObject(resource: String, type: String) -> Data
}
extension MockJsonService {
    func buildObject(resource: String, type: String = "json") -> Data {
        let path = Bundle.main.path(forResource: resource, ofType: type)
        if path == nil {
            fatalError(".json file doesn't exists")
        }
        let data = try! Data(contentsOf: URL(fileURLWithPath: path!))
        return data
    }
}
