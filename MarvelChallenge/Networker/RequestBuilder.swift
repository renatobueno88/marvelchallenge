//
//  RequestBuilder.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 22/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

protocol RequestParameterFormatterProtocol {
    func formatParamsToQueryString() -> String
}

struct RequestBuilder: RequestParameterFormatterProtocol {
    var parameters = JSONDictionary()
    
    func formatParamsToQueryString() -> String {
        return parameters.compactMap({ "\($0.key)\($0.value)" }).reduce("", { $0 + $1 })
    }
}
