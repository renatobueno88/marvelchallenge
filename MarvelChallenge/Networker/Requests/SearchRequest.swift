//
//  SearchRequest.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 23/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

enum SearchTermType {
    case name
    case nameStartsWith
    
    var queryKey: String {
        switch self {
        case .name:
            return "&name="
        case .nameStartsWith:
            return "&nameStartsWith="
        }
    }
}

protocol SearchRequestService {
    init()
    func fetchCharactersFromSearch(searchTerm: String, searchTermType: SearchTermType, paging: Int, limit: Int, completion: @escaping (_ object: ResultObject?, _ error: Error?) -> ())
}

class SearchRequest: DecodableProtocol, SearchRequestService {
    
    private var requestableType: NetworkerProtocol.Type
    private var requestable: NetworkerProtocol {
        return requestableType.init(urlString: Environment.baseUrl)
    }
    
    init(requestable: NetworkerProtocol.Type = NetworkRequest.self) {
        self.requestableType = requestable
    }
    
    // MARK: SearchRequestService
    
    required convenience init() {
        self.init(requestable: NetworkRequest.self)
    }
    
    func fetchCharactersFromSearch(searchTerm: String, searchTermType: SearchTermType, paging: Int, limit: Int, completion: @escaping (ResultObject?, Error?) -> ()) {
        let text = searchTerm.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? searchTerm
        let requestBuilder = RequestBuilder(parameters: ["&offset=": paging, "&limit=": limit, "&orderBy=": "name", searchTermType.queryKey: text])
        requestable.makeRequest(action: URLAction.characters, method: .get, builder: requestBuilder) { (response) in
            self.decodeObject(data: response?.rawData, error: response?.error, type: ResultObject.self, completion: completion)
        }
    }
    
}
