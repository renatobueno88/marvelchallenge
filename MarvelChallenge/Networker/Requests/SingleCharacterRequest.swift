//
//  SingleCharacterRequest.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 25/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

protocol SingleCharacterRequestService {
    init()
    func fetchSingleCharacter(characterId: Int, completion: @escaping (_ object: ResultObject?, _ error: Error?) -> ())
}

class SingleCharacterRequest: SingleCharacterRequestService, DecodableProtocol {
    
    private var requestableType: NetworkerProtocol.Type
    private var requestable: NetworkerProtocol {
        return requestableType.init(urlString: Environment.baseUrl)
    }
    
    init(requestable: NetworkerProtocol.Type = NetworkRequest.self) {
        self.requestableType = requestable
    }
    
    // MARK: SingleCharacterRequestService
    
    required convenience init() {
        self.init(requestable: NetworkRequest.self)
    }
    
    func fetchSingleCharacter(characterId: Int, completion: @escaping (_ object: ResultObject?, _ error: Error?) -> ()) {
        let requestBuilder = RequestBuilder(parameters: [:])
        requestable.makeRequest(action: URLAction.singleCharacter(id: characterId), method: .get, builder: requestBuilder) { (response) in
            self.decodeObject(data: response?.rawData, error: response?.error, type: ResultObject.self, completion: completion)
        }
    }
}
