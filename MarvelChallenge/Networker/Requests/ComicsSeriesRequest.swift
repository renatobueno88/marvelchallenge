//
//  ComicsSeriesRequest.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 24/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

enum MarvelProductsType: CaseIterable {
    case comics
    case series
}

protocol ComicsSeriesService {
    init()
    func fetchItemsFromCharacter(characterId: Int, marvelProductsType: MarvelProductsType, paging: Int, limit: Int, completion: @escaping (_ object: ResultObject?, _ error: Error?) -> ())
}

class ComicsSeriesRequest: DecodableProtocol, ComicsSeriesService {
    
    private var requestableType: NetworkerProtocol.Type
    private var requestable: NetworkerProtocol {
        return requestableType.init(urlString: Environment.baseUrl)
    }
    
    init(requestable: NetworkerProtocol.Type = NetworkRequest.self) {
        self.requestableType = requestable
    }
    
    // MARK: ComicsSeriesService
    
    required convenience init() {
        self.init(requestable: NetworkRequest.self)
    }
    
    private func getActionType(fromType: MarvelProductsType, id: Int) -> URLAction {
        switch fromType {
        case .comics:
            return URLAction.comics(id: id)
        case .series:
            return URLAction.series(id: id)
        }
    }
    
    func fetchItemsFromCharacter(characterId: Int, marvelProductsType: MarvelProductsType, paging: Int, limit: Int, completion: @escaping (_ object: ResultObject?, _ error: Error?) -> ()) {
        
        let requestBuilder = RequestBuilder(parameters: ["&offset=": paging, "&limit=": limit])
        let action = getActionType(fromType: marvelProductsType, id: characterId)
        
        requestable.makeRequest(action: action, method: .get, builder: requestBuilder) { (response) in
            self.decodeObject(data: response?.rawData, error: response?.error, type: ResultObject.self, completion: completion)
        }
    }
}
