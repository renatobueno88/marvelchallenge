//
//  FavoriteLocalRequest.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 23/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

protocol FavoriteLocalProtocol {
    init(favoriteKey: String)
    func saveObject(object: [MarvelObject])
    func queryObject() -> [MarvelObject]?
    func querySingleObject(id: Int) -> MarvelObject?
    func removeAllObjects() -> Bool
}

class FavoriteLocalService: LocalService, FavoriteLocalProtocol {
    
    private var favoriteKey = ""
    
    // MARK: FavoriteLocalProtocol
    
    required init(favoriteKey: String) {
        self.favoriteKey = favoriteKey
    }
    
    func saveObject(object: [MarvelObject]) {
        saveObject(withKey: favoriteKey, object: object)
    }
    
    func queryObject() -> [MarvelObject]? {
        return queryObject(withKey: favoriteKey)
    }
    
    func querySingleObject(id: Int) -> MarvelObject? {
        return queryObject()?.first(where: { $0.id == id })
    }
    
    func removeAllObjects() -> Bool {
        return removeObject(withKey: favoriteKey)
    }
    
    // MARK: LocalService
    
    typealias LocalObject = [MarvelObject]
    
    init() {
        
    }
    
    func removeObject(withKey key: String) -> Bool {
        if queryObject(withKey: key) != nil {
            UserDefaults.standard.removeObject(forKey: key)
            return true
        }
        return false
    }
    
    func saveObject(withKey key: String, object: [MarvelObject]) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(object) {
            UserDefaults.standard.set(encoded, forKey: key)
        }
    }
    
    func queryObject(withKey key: String) -> [MarvelObject]? {
        if let data = UserDefaults.standard.data(forKey: key) {
            let decoder = JSONDecoder()
            if let object = try? decoder.decode(LocalObject.self, from: data) {
                return object
            } else {
                return nil
            }
        } else {
            return nil
        }
    }
    
    
}
