//
//  SharedFavoriteMocked.swift
//  MarvelChallengeTests
//
//  Created by Renato Souza Bueno on 25/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

class SharedFavoriteMocked {
    static let shared = SharedFavoriteMocked()
    
    init() {
    }
    
    var favorites = [MarvelObject]()
}
