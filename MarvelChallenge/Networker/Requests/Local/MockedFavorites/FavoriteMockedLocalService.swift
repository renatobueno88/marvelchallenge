//
//  FavoriteMockedLocalService.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 25/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

class FavoriteMockedLocalService: LocalService, FavoriteLocalProtocol {
    
    private var favoriteKey = ""
    
    // MARK: FavoriteLocalProtocol
    
    required init(favoriteKey: String) {
        self.favoriteKey = favoriteKey
    }
    
    func saveObject(object: [MarvelObject]) {
        saveObject(withKey: favoriteKey, object: object)
    }
    
    func queryObject() -> [MarvelObject]? {
        return queryObject(withKey: favoriteKey)
    }
    
    func querySingleObject(id: Int) -> MarvelObject? {
        return queryObject()?.first(where: { $0.id == id })
    }
    
    func removeAllObjects() -> Bool {
        return removeObject(withKey: favoriteKey)
    }
    
    // MARK: LocalService
    
    typealias LocalObject = [MarvelObject]
    
    init() {
        
    }
    
    func removeObject(withKey key: String) -> Bool {
        return true
    }
    
    func saveObject(withKey key: String, object: [MarvelObject]) {
        SharedFavoriteMocked.shared.favorites.append(contentsOf: object)
    }
    
    func queryObject(withKey key: String) -> [MarvelObject]? {
        return SharedFavoriteMocked.shared.favorites
    }

}
