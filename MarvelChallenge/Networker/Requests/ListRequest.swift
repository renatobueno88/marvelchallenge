//
//  ListRequest.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 22/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

protocol ListRequestService {
    init()
    func fetchCharacters(paging: Int, limit: Int, completion: @escaping (_ object: ResultObject?, _ error: Error?) -> ())
}

class ListRequest: DecodableProtocol, ListRequestService {
    
    private var requestableType: NetworkerProtocol.Type
    private var requestable: NetworkerProtocol {
        return requestableType.init(urlString: Environment.baseUrl)
    }
    
    init(requestable: NetworkerProtocol.Type = NetworkRequest.self) {
        self.requestableType = requestable
    }
    
    // MARK: ListRequestService
    
    required convenience init() {
        self.init(requestable: NetworkRequest.self)
    }
    
    func fetchCharacters(paging: Int, limit: Int, completion: @escaping (_ object: ResultObject?, _ error: Error?) -> ()) {
        let requestBuilder = RequestBuilder(parameters: ["&offset=": paging, "&limit=": limit, "&orderBy=": "name"])
        requestable.makeRequest(action: URLAction.characters, method: .get, builder: requestBuilder) { (response) in
            self.decodeObject(data: response?.rawData, error: response?.error, type: ResultObject.self, completion: completion)
        }
    }
    
}
