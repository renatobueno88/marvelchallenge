//
//  ComicsSeriesMockedRequest.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 26/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

class ComicsSeriesMockedRequest: DecodableProtocol, ComicsSeriesService {
    
    private var requestableType: NetworkerProtocol.Type
    private var requestable: NetworkerProtocol {
        return requestableType.init(urlString: "")
    }
    
    init(requestable: NetworkerProtocol.Type = MockedRequest.self) {
        self.requestableType = requestable
    }
    
    // MARK: ListRequestService
    
    required convenience init() {
        self.init(requestable: MockedRequest.self)
    }
    
    private func getActionType(fromType: MarvelProductsType, id: Int) -> MockedAction {
        switch fromType {
        case .comics:
            return MockedAction.comics(id: id)
        case .series:
            return MockedAction.series(id: id)
        }
    }
    
    func fetchItemsFromCharacter(characterId: Int, marvelProductsType: MarvelProductsType, paging: Int, limit: Int, completion: @escaping (ResultObject?, Error?) -> ()) {
        let action = getActionType(fromType: marvelProductsType, id: characterId)
        requestable.makeRequest(action: action, method: .get, builder: RequestBuilder()) { (response) in
            self.decodeObject(data: response?.rawData, error: response?.error, type: ResultObject.self, completion: completion)
        }
    }
}
