//
//  SingleCharacterMockedRequest.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 26/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

class SingleCharacterMockedRequest: SingleCharacterRequestService, DecodableProtocol {
    
    private var requestableType: NetworkerProtocol.Type
    private var requestable: NetworkerProtocol {
        return requestableType.init(urlString: "")
    }
    
    init(requestable: NetworkerProtocol.Type = MockedRequest.self) {
        self.requestableType = requestable
    }
    
    // MARK: SingleCharacterRequestService
    
    required convenience init() {
        self.init(requestable: MockedRequest.self)
    }
    
    func fetchSingleCharacter(characterId: Int, completion: @escaping (ResultObject?, Error?) -> ()) {
        let requestBuilder = RequestBuilder(parameters: [:])
        requestable.makeRequest(action: MockedAction.singleCharacter(id: characterId), method: .get, builder: requestBuilder) { (response) in
            self.decodeObject(data: response?.rawData, error: response?.error, type: ResultObject.self, completion: completion)
        }
    }
    
}
