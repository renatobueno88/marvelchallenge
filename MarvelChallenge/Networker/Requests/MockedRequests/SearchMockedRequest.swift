//
//  SearchMockedRequest.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 26/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

class SearchMockedRequest: DecodableProtocol, SearchRequestService {
    
    private var requestableType: NetworkerProtocol.Type
    private var requestable: NetworkerProtocol {
        return requestableType.init(urlString: "")
    }
    
    init(requestable: NetworkerProtocol.Type = MockedRequest.self) {
        self.requestableType = requestable
    }
    
    // MARK: SearchRequestService
    
    required convenience init() {
        self.init(requestable: MockedRequest.self)
    }
    
    func fetchCharactersFromSearch(searchTerm: String, searchTermType: SearchTermType, paging: Int, limit: Int, completion: @escaping (ResultObject?, Error?) -> ()) {
        let text = searchTerm.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? searchTerm
        let requestBuilder = RequestBuilder(parameters: ["&offset=": paging, "&limit=": limit, "&orderBy=": "name", searchTermType.queryKey: text])
        requestable.makeRequest(action: MockedAction.characters, method: .get, builder: requestBuilder) { (response) in
            self.decodeObject(data: response?.rawData, error: response?.error, type: ResultObject.self, completion: completion)
        }
    }
    
    
}
