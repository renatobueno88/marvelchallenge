//
//  ListMockedRequest.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 25/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

class ListMockedRequest: ListRequestService, DecodableProtocol {
    
    private var requestableType: NetworkerProtocol.Type
    private var requestable: NetworkerProtocol {
        return requestableType.init(urlString: "")
    }
    
    init(requestable: NetworkerProtocol.Type = MockedRequest.self) {
        self.requestableType = requestable
    }
    
    // MARK: ListRequestService
    
    required convenience init() {
        self.init(requestable: MockedRequest.self)
    }
    
    func fetchCharacters(paging: Int, limit: Int, completion: @escaping (ResultObject?, Error?) -> ()) {
        requestable.makeRequest(action: MockedAction.characters, method: .get, builder: RequestBuilder()) { (response) in
            self.decodeObject(data: response?.rawData, error: response?.error, type: ResultObject.self, completion: completion)
        }
    }
}
