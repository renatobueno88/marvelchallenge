//
//  Environment.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 22/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation

protocol ActionProtocol {
    var actionKey: String { get }
}

enum URLAction: ActionProtocol {
    case characters
    case comics(id: Int)
    case series(id: Int)
    case singleCharacter(id: Int)
    
    var actionKey: String {
        switch self {
        case .characters:
            return "characters"
        case .comics(id: let id):
            return "characters/\(id)/comics"
        case .series(id: let id):
            return "characters/\(id)/series"
        case .singleCharacter(id: let id):
            return "characters/\(id)"
        }
    }
}

struct Environment {
    static let schemeUrl = "MARVEL-CHALLENGE://"
    static let baseUrl = "http://gateway.marvel.com/v1/public/"
    let privateKey = "b5e1f463fbcacb892d06923b1dde07813e061534"
    let publicKey = "d57edaad7b698a9fec4609244d502e4d"
}
