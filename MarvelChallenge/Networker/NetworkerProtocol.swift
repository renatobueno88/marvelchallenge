//
//  NetworkerProtocol.swift
//  MarvelChallenge
//
//  Created by Renato Souza Bueno on 22/05/19.
//  Copyright © 2019 Renato Souza Bueno. All rights reserved.
//

import Foundation
import CommonCrypto

protocol NetworkerProtocol {
    init(urlString: String)
    func makeRequest(action: ActionProtocol, method: HTTPMethodType, builder: RequestBuilder, completion: @escaping (HTTPResponse?) -> ())
    func defaultQueryParameters() -> String
}

extension NetworkerProtocol {
    
    func defaultQueryParameters() -> String {
        let environment = Environment()
        let timeStamp = Date().timeIntervalSince1970.description
        let concatValues = "\(timeStamp)\(environment.privateKey)\(environment.publicKey)"
        let hashString = concatValues.MD5() ?? ""
        let baseURL = "ts=\(timeStamp)&apikey=\(environment.publicKey)&hash=\(hashString)"
        return baseURL
    }
}

fileprivate extension String {
    func MD5() -> String? {
        let length = Int(CC_MD5_DIGEST_LENGTH)
        var digest = [UInt8](repeating: 0, count: length)
        
        if let d = self.data(using: String.Encoding.utf8) {
            _ = d.withUnsafeBytes { (body: UnsafePointer<UInt8>) in
                CC_MD5(body, CC_LONG(d.count), &digest)
            }
        }
        
        return (0..<length).reduce("") {
            $0 + String(format: "%02x", digest[$1])
        }
    }
}
